import cv2
import numpy as np
from  matplotlib import pyplot as plt
from matplotlib import pyplot as plt



def nothing(x):
  pass
cv2.namedWindow('Takvaviya')


cv2.createTrackbar("R1", "Takvaviya",0,255,nothing)
cv2.createTrackbar("G1", "Takvaviya",0,255,nothing)
cv2.createTrackbar("B1", "Takvaviya",0,255,nothing)
cv2.createTrackbar("R2", "Takvaviya",0,255,nothing)
cv2.createTrackbar("G2", "Takvaviya",0,255,nothing)
cv2.createTrackbar("B2", "Takvaviya",0,255,nothing)
cv2.createTrackbar("area1", "Takvaviya",0,200000,nothing)
cv2.createTrackbar("area2", "Takvaviya",0,200000,nothing)


img = cv2.imread('/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Syn_RM_Bounding_Box/Overlapped_row_small.tif')

img = cv2.resize(img, (0,0), fx=7, fy=7)



while(1):
   R1=cv2.getTrackbarPos("R1", "Takvaviya")
   G1=cv2.getTrackbarPos("G1", "Takvaviya")
   B1 = cv2.getTrackbarPos("B1", "Takvaviya")
   R2 = cv2.getTrackbarPos("R2", "Takvaviya")
   G2 = cv2.getTrackbarPos("G2", "Takvaviya")
   B2 = cv2.getTrackbarPos("B2", "Takvaviya")
   area1 =cv2.getTrackbarPos("area1", "Takvaviya")
   area2 = cv2.getTrackbarPos("area1", "Takvaviya")

   hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
   mask = cv2.inRange(hsv, (R1, G1, B1), (R2, G2, B2))
   imask = mask > 0
   green = np.zeros_like(img, np.uint8)
   green[imask] = img[imask]
   gray = cv2.cvtColor(green, cv2.COLOR_BGR2GRAY)
   ret, threshold = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
   source  = cv2.imread('/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Syn_RM_Bounding_Box/crop_small.tif')
   contours, _ = cv2.findContours(threshold, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

   for cnt in contours:
      area = cv2.contourArea(cnt)
      if (5 < area < 2000):
         wx, wy, ww, wh = cv2.boundingRect(cnt)
         cv2.rectangle(source, (wx, wy), (wx + ww, wy + wh), (0, 255, 0), 2)


   cv2.imshow("green", source)




   k = cv2.waitKey(1) & 0xFF
   if k == ord('m'):
     mode = not mode
   elif k == 27:
     break
cv2.destroyAllWindows()

# plt.imshow(cv2.cvtColor(green, cv2.COLOR_BGR2RGB)), plt.show()
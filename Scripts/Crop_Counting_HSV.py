
##============================== import packages ===========================##
import cv2
import numpy as np
from matplotlib import pyplot as plt


##============================== Creat a ASK window =========================##

import tkinter as tk
from tkinter import filedialog
root = tk.Tk()
root.withdraw()
file_path = filedialog.askopenfilename()


## ==============================  Read =======================================##
img = cv2.imread(file_path)


## ==============================  convert to hsv =======================================##
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)


## ============================== mask of green  =======================================##
mask = cv2.inRange(hsv, (19, 43 , 0), (53, 255, 255))



## ============================== slice the green  =======================================##
imask = mask>0
green = np.zeros_like(img, np.uint8)
green[imask] = img[imask]


cv2.imwrite("green.jpg", green)

## ============================== Contours detection  =======================================##

final = cv2.imread('green.jpg')
source = cv2.imread('cobbWI_RGB_Crop.jpg')
gray = cv2.cvtColor(final,cv2.COLOR_BGR2GRAY)
ret, threshold = cv2.threshold(gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

_,contours,_ = cv2.findContours(threshold, cv2.RETR_LIST,cv2.CHAIN_APPROX_NONE)


n = 0
for cnt in contours:
	area = cv2.contourArea(cnt)

	if (15 < area <500):
		x,y,w,h = cv2.boundingRect(cnt)
		cv2.rectangle(source,(x,y),(x+w,y+h),(0,255,0),2)
		#roi = source[y:y + h, x:x + w]
		#cv2.imwrite('Image_crop%d.jpg'%n, roi)
		n+=1


##============================== Show the image ====================================##
cv2.imwrite('Detected Row Crops.jpg',source)

plt.title('Number of Row Crops Detected = %d'%n)
plt.imshow(source)
plt.show()

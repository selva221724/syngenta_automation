from datetime import datetime
start_time = datetime.now()

# ================================== Import Packages ======================================================
from pykml import parser
import simplekml
import cv2
from matplotlib import pyplot as plt
import numpy as np
import gdal
from pyproj import Proj, transform
from skimage import io, img_as_float
from skimage.io import imread
from matplotlib import pyplot as plt

cv2.useOptimized()





# =================================== Reading the Image and KML ==========================================

# file_path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_082218.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_090818.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_091118.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_091418.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_091718.tif'
# file_path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_091918.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_092218.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_092518.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_092618.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONVTEST_092818.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONVTEST_100218.tif'
file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_100418.tif'




img = imread(file_path)
source = img

kml_file = "/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/grid/grid.kml"
f = open(kml_file, "r")
docs = parser.parse(f)
doc = docs.getroot().Document.Folder
print('number of features in KML is', len(doc.Placemark))


# ===================================== Load KML and append the Data in variable ============================

ID = []
coords = []
for place in doc.Placemark:
    x = str(place.Polygon.outerBoundaryIs.LinearRing.coordinates)
    x = x.strip().split("\n")
    x = [i.strip() for i in x]
    m = 1
    for i in x:
        i = i.replace(' ', ',')
        i = i.split(",")
        co = [[float(i[0]), float(i[1])], [float(i[3]), float(i[4])], [float(i[6]), float(i[7])], [float(i[9]), float(i[10])]]
        coords.append(co)

    y = str(place.ExtendedData.SchemaData.SimpleData)
    ID.append(y)


print('number of Coordinates Extracted from KML is',len(coords))


# ======================================= Store the coordinates is the List ===================================

global_coord = []
for i in range(len(coords)):
    coordinates = [(coords[i][2][0], coords[i][2][1]), (coords[i][0][0], coords[i][0][1])]
    global_coord.append(coordinates)


# ======================================== Change the Geo_projection of the Coordinated =============================
inProj = Proj("+init=EPSG:4326", preserve_units=True)
outProj = Proj("+init=EPSG:32615", preserve_units=True)

global_coord_conv=[]
for i in range(len(global_coord)):

    gcc1 = transform(inProj, outProj, global_coord[i][0][0], global_coord[i][0][1])
    gcc2 = transform(inProj, outProj, global_coord[i][1][0], global_coord[i][1][1])
    global_coord_conv.append([gcc1,gcc2])


# ======================================== Load the image in GDAL and Get geoTransformation Values ==================

srcImage = gdal.Open(file_path)
geoTrans = srcImage.GetGeoTransform()


def world2Pixel(geoMatrix, x, y):
  ulX = geoMatrix[0]
  ulY = geoMatrix[3]
  xDist = geoMatrix[1]
  yDist = geoMatrix[5]
  rtnX = geoMatrix[2]
  rtnY = geoMatrix[4]
  pixel = int((x - ulX) / xDist)
  line = int((ulY - y) / xDist)
  return (pixel, line)


def Pixel2world(geoMatrix, x, y):
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    yDist = geoMatrix[5]
    coorX = (ulX + (x * xDist))
    coorY = (ulY + (y * yDist))
    return (coorX, coorY)


# ============================ Converting the Global Coordinates into the Pixel Coordinates ==========================

pixel_coord =[]
for i in range(len(global_coord_conv)):
    x,y = world2Pixel(geoTrans, global_coord_conv[i][0][0], global_coord_conv[i][0][1])
    g,h = world2Pixel(geoTrans, global_coord_conv[i][1][0], global_coord_conv[i][1][1])
    pixel_coord.append([(x, y), (g, h)])


# ================================== Iterate the Boxes into the Image Processing ======================================

pixel_value = []
iter_el = []

x1=[]
y1=[]
x2=[]
y2=[]

for i in range(len(pixel_coord)):

    crop_img = img[pixel_coord[i][1][1]:pixel_coord[i][0][1], pixel_coord[i][1][0]:pixel_coord[i][0][0]]
    hsv = cv2.cvtColor(crop_img, cv2.COLOR_RGB2HSV)
    mask = cv2.inRange(hsv, (0, 0, 0), (255, 90, 255))
    imask = mask > 0
    green = np.zeros_like(crop_img, np.uint8)
    green[imask] = crop_img[imask]
    gray = cv2.cvtColor(green, cv2.COLOR_BGR2GRAY)
    ret, threshold = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    contours, _ = cv2.findContours(threshold, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

    area=[]
    for n in range(len(contours)):
        area.append(cv2.contourArea(contours[n]))
    max = sum(area)

    h,w,g = crop_img.shape
    img_area = h*w
    percentage = (max/img_area)*100

    if percentage > 3 :
        x1.append(pixel_coord[i][0][0])
        y1.append(pixel_coord[i][0][1])
        x2.append(pixel_coord[i][1][0])
        y2.append(pixel_coord[i][1][1])
        iter_el.append(i)



from operator import itemgetter

grid_id = itemgetter(*iter_el)(ID)

from pathlib import Path
v = Path(file_path).name
v = v.replace(' ','_')
file = v.replace(".", " ").split()[0]

import pandas as pd
#
# data = pd.DataFrame({file:grid_id})
# data.to_csv("RM_ID.csv",index=False)


#
dataset = pd.read_csv("RM_ID.csv")
g= pd.DataFrame({file:grid_id})
dataset = pd.concat([dataset,g], axis=1)
dataset.to_csv("RM_ID.csv",index=False)

print('Mature Count is '+ str(len(x1)))
## ======================================== Drawing Boxes on the Source Image =====================================
#
# for i in range(len(x1)):
#     cv2.rectangle(source, (x1[i], y1[i]), (x2[i], y2[i]), (0, 255, 0), 2)
#
# plt.imshow(cv2.cvtColor(source, cv2.COLOR_BGR2RGB)), plt.show()
#
#
## ============================== Converting the Pixel Coordinates into the Global Coordinates======================
#
# def set_crs(x, y):
#     inProj = Proj(init='epsg:32615')
#     outProj = Proj(init='epsg:4326')
#     x2, y2 = transform(inProj, outProj, x, y)
#     return x2, y2
#
#
# geo_x = []
# geo_y = []
# geo_x1 = []
# geo_y1 = []
# for i in range(len(x1)):
#     n, m = Pixel2world(geoTrans, x1[i], y1[i])
#     o, p = Pixel2world(geoTrans, x2[i],  y2[i])
#     n, m = set_crs(n, m)
#     o, p = set_crs(o, p)
#     geo_x.append(n)
#     geo_y.append(m)
#     geo_x1.append(o)
#     geo_y1.append(p)
#
#
# final_coordinates = []
# for i in range(len(geo_x)):
#     final_coordinates.append([geo_x[i], geo_y[i], geo_x1[i], geo_y1[i]])
#
# print('Mature Count is '+ str(len(final_coordinates)))
#
# kml = simplekml.Kml()
# for row in final_coordinates:
#     # print(row)
#     pol = kml.newpolygon(outerboundaryis=[(row[0], row[1]),
#                                                          (row[2], row[1]),
#                                                          (row[2], row[3]),
#                                                          (row[0], row[3]),
#                                                          (row[0], row[1])])
#
# from pathlib import Path
# v = Path(file_path).name
# file = v.replace(".", " ").split()[0]
# kml.save(file+'.kml')



# print(len(final_coordinates))
#
# print('KML Exported Successfully')
#
# end_time = datetime.now()
# print('Total Duration of the Execution is {}'.format(end_time - start_time), "seconds")


#
# a = [1,48,702,1498,2111,6277,9346,12139,15288,15580,16151]
#
# b = [0,49,2173,7960,6733,9723,12970,13156,13984,13139,14988,15695]
#
# x = range(len(b))
# y = b
# plt.xlabel('Fly Date', fontsize=16)
# plt.ylabel('Grid Count', fontsize=16)
# my_xticks = ['22nd AUG', '8th SEP','11th SEP','14th SEP', '17th SEP','19th SEP', '22nd SEP', '25th SEP', '26th SEP' , '28th SEP' , '2nd OCT','4th OCT' ]
# plt.xticks(x, my_xticks)
# plt.plot(x, y)
# plt.show()
#
#
# x = range(len(a))
# y = a
# plt.xlabel('Fly Date', fontsize=16)
# plt.ylabel('Grid Count', fontsize=16)
# my_xticks = ['22nd AUG', '8th SEP','11th SEP','14th SEP', '17th SEP', '22nd SEP', '25th SEP', '26th SEP' , '28th SEP' , '2nd OCT','4th OCT' ]
# plt.xticks(x, my_xticks)
# plt.plot(x, y)
# plt.show()



fig = plt.figure()
ax = fig.add_subplot(111)
plt.title('Grid Count vs Fly Date (Cumulative)',fontsize=18,fontweight=10)
values = [0,623,1220,2835,3485,5203,7569,10156,12194,13937,15341,16151]
labels = ['22nd AUG', '8th SEP','11th SEP','14th SEP', '17th SEP', '19th SEP', '22nd SEP', '25th SEP', '26th SEP' , '28th SEP' , '2nd OCT','4th OCT' ]

plt.xlabel('Fly Date', fontsize=16)
plt.ylabel('Grid Count', fontsize=16)
plt.xticks(range(len(labels)), labels) # Redefining x-axis labels

for i, v in enumerate(values):
    ax.text(i, v+300, "%d" %v, ha="center")
plt.axhline(15960, color='b', linestyle='--')
plt.axvline(10.8, color='b', linestyle='--')
plt.plot(range(len(labels)), values,'-gD', marker='.') # Plotting data


##============================ INCREMENTAL DATA ===========================================
# dataset = pd.read_csv("RM_ID.csv")
# a = dataset['CONV_091118'].to_list()
# b = dataset['CONV_090818'].to_list()
# k = list(set(a).difference(set(b)))
# len(k)
#
# kk = pd.DataFrame(k)
#
# jk = pd.read_csv("RM_ID_ITER.csv")
# data = pd.DataFrame({'CONV_091118':k})
# data = pd.concat([jk,data], axis=1)
# data.to_csv("RM_ID_ITER.csv",index=False)

#============================= PLOT ======================================================


fig = plt.figure()
ax = fig.add_subplot(111)
plt.title('Grid Count vs Fly Date (Incremental)',fontsize=18,fontweight=10)
values = [0,623,598,1615,817,1860,2448,2594,2047,1758,1439,815]
labels = ['22nd AUG', '8th SEP','11th SEP','14th SEP', '17th SEP', '19th SEP', '22nd SEP', '25th SEP', '26th SEP' , '28th SEP' , '2nd OCT','4th OCT' ]

plt.xlabel('Fly Date', fontsize=16)
plt.ylabel('Grid Count', fontsize=16)
plt.xticks(range(len(labels)), labels) # Redefining x-axis labels

for i, v in enumerate(values):
    ax.text(i, v+50, "%d" %v, ha="center")
plt.axhline(2594, color='b', linestyle='--')
plt.axvline(7, color='b', linestyle='--')
plt.plot(range(len(labels)), values,'-gD', marker='.') # Plotting data
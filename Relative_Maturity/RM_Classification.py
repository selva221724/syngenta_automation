from datetime import datetime
start_time = datetime.now()

# ================================== Import Packages ======================================================
import pykml
from pykml import parser
import simplekml
import cv2
from matplotlib import pyplot as plt
import numpy as np
import gdal
from pyproj import Proj, transform
from skimage import io, img_as_float

cv2.useOptimized()


# =================================== Reading the Image and KML ==========================================

file_path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Syn_RM_Bounding_Box/Input_raster/2nd_OCT_2018_Cropped.tif'
img = cv2.imread(file_path)
source = img

kml_file = "/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Syn_RM_Bounding_Box/Input_kml/10_02_18_shifted.kml"
f = open(kml_file, "r")
docs = parser.parse(f)
doc = docs.getroot().Document.Folder
print('number of features in KML is', len(doc.Placemark))


# ===================================== Load KML and append the Data in variable ============================

polygons = []
coords = []
for place in doc.Placemark:
    x = str(place.Polygon.outerBoundaryIs.LinearRing.coordinates)
    x = x.strip().split("\n")
    x = [i.strip() for i in x]
    m = 1
    for i in x:
        i = i.replace(' ', ',')
        i = i.split(",")
        co = [[float(i[0]), float(i[1])], [float(i[3]), float(i[4])], [float(i[6]), float(i[7])], [float(i[9]), float(i[10])]]
        coords.append(co)
print('number of Coordinates Extracted from KML is',len(coords))

# ======================================= Store the coordinates is the List ===================================

global_coord = []
for i in range(len(coords)):
    coordinates = [(coords[i][1][0], coords[i][1][1]), (coords[i][3][0], coords[i][3][1])]
    global_coord.append(coordinates)


# ======================================== Change the Geo_projection of the Coordinated =============================
inProj = Proj("+init=EPSG:4326", preserve_units=True)
outProj = Proj("+init=EPSG:32615", preserve_units=True)

global_coord_conv=[]
for i in range(len(global_coord)):

    gcc1 = transform(inProj, outProj, global_coord[i][0][0], global_coord[i][0][1])
    gcc2 = transform(inProj, outProj, global_coord[i][1][0], global_coord[i][1][1])
    global_coord_conv.append([gcc1,gcc2])


# ======================================== Load the image in GDAL and Get geoTransformation Values ==================

srcImage = gdal.Open(file_path)
geoTrans = srcImage.GetGeoTransform()


def world2Pixel(geoMatrix, x, y):
  ulX = geoMatrix[0]
  ulY = geoMatrix[3]
  xDist = geoMatrix[1]
  yDist = geoMatrix[5]
  rtnX = geoMatrix[2]
  rtnY = geoMatrix[4]
  pixel = int((x - ulX) / xDist)
  line = int((ulY - y) / xDist)
  return (pixel, line)


def Pixel2world(geoMatrix, x, y):
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    yDist = geoMatrix[5]
    coorX = (ulX + (x * xDist))
    coorY = (ulY + (y * yDist))
    return (coorX, coorY)


# ============================ Converting the Global Coordinates into the Pixel Coordinates ==========================

pixel_coord =[]
for i in range(len(global_coord_conv)):
    x,y = world2Pixel(geoTrans, global_coord_conv[i][0][0], global_coord_conv[i][0][1])
    g,h = world2Pixel(geoTrans, global_coord_conv[i][1][0], global_coord_conv[i][1][1])
    pixel_coord.append([(x, y), (g, h)])


# ================================== Iterate the Boxes into the Image Processing ======================================

pixel_value = []

count = 0
for i in range(len(pixel_coord)):

    crop_img = img[pixel_coord[i][0][1]:pixel_coord[i][1][1], pixel_coord[i][0][0]:pixel_coord[i][1][0]]
    green = crop_img[:, :, 1]
    green_float_2 = img_as_float(green)
    crop_2 = np.mean(green_float_2)
    pixel_value.append(crop_2)
    count+=1

print('number of Image Processed-Boxes generated is', count)


band = max(pixel_value)-min(pixel_value)
rm_interval = band/9
band_1 = min(pixel_value) + rm_interval
band_2 = min(pixel_value) + (2*rm_interval)
band_3 = min(pixel_value) + (3*rm_interval)
band_4 = min(pixel_value) + (4*rm_interval)
band_5 = min(pixel_value) + (5*rm_interval)
band_6 = min(pixel_value) + (6*rm_interval)
band_7 = min(pixel_value) + (7*rm_interval)
band_8 = min(pixel_value) + (8*rm_interval)
band_9 = min(pixel_value) + (9*rm_interval)


x1=[]
y1=[]
x2=[]
y2=[]

# pixel_value = []

for i in range(len(pixel_value)):
    if band_4 < pixel_value[i] < band_5 :
        x1.append(pixel_coord[i][0][0] + 6)
        y1.append(pixel_coord[i][0][1] + 6)
        x2.append(pixel_coord[i][1][0] - 6)
        y2.append(pixel_coord[i][1][1] - 6)





# ======================================== Drawing Boxes on the Source Image =====================================

for i in range(len(x1)):
    cv2.rectangle(source, (x1[i], y1[i]), (x2[i], y2[i]), (0, 255, 0), 2)

# plt.imshow(cv2.cvtColor(source, cv2.COLOR_BGR2RGB)), plt.show()


# ============================== Converting the Pixel Coordinates into the Global Coordinates======================

def set_crs(x, y):
    inProj = Proj(init='epsg:32615')
    outProj = Proj(init='epsg:4326')
    x2, y2 = transform(inProj, outProj, x, y)
    return x2, y2


geo_x = []
geo_y = []
geo_x1 = []
geo_y1 = []
for i in range(len(x1)):
    n, m = Pixel2world(geoTrans, x1[i], y1[i])
    o, p = Pixel2world(geoTrans, x2[i],  y2[i])
    n, m = set_crs(n, m)
    o, p = set_crs(o, p)
    geo_x.append(n)
    geo_y.append(m)
    geo_x1.append(o)
    geo_y1.append(p)


final_coordinates = []
for i in range(len(geo_x)):
    final_coordinates.append([geo_x[i], geo_y[i], geo_x1[i], geo_y1[i]])


kml = simplekml.Kml()
for row in final_coordinates:
    print(row)
    pol = kml.newpolygon(name='Syngenta', outerboundaryis=[(row[0], row[1]),
                                                         (row[2], row[1]),
                                                         (row[2], row[3]),
                                                         (row[0], row[3]),
                                                         (row[0], row[1])])

kml.save('Syngenta_RM_5.kml')

print('KML Exported Successfully')

end_time = datetime.now()
print('Total Duration of the Execution is {}'.format(end_time - start_time), "seconds")

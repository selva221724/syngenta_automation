from datetime import datetime
start_time = datetime.now()

# ================================== Import Packages ======================================================
import pykml
from pykml import parser
import simplekml
import cv2
from matplotlib import pyplot as plt
import numpy as np
import gdal
from pyproj import Proj, transform
cv2.useOptimized()


# =================================== Reading the Image and KML ==========================================

file_path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Syn_RM_Bounding_Box/Input_raster/2nd_OCT_2018_Cropped.tif'
img = cv2.imread(file_path)
source = img

kml_file = "/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Syn_RM_Bounding_Box/Input_kml/10_02_18_shifted.kml"
f = open(kml_file, "r")
docs = parser.parse(f)
doc = docs.getroot().Document.Folder
print('number of features in KML is', len(doc.Placemark))


# ===================================== Load KML and append the Data in variable ============================

polygons = []
coords = []
for place in doc.Placemark:
    x = str(place.Polygon.outerBoundaryIs.LinearRing.coordinates)
    x = x.strip().split("\n")
    x = [i.strip() for i in x]
    m = 1
    for i in x:
        i = i.replace(' ', ',')
        i = i.split(",")
        co = [[float(i[0]), float(i[1])], [float(i[3]), float(i[4])], [float(i[6]), float(i[7])], [float(i[9]), float(i[10])]]
        coords.append(co)
print('number of Coordinates Extracted from KML is',len(coords))

# ======================================= Store the coordinates is the List ===================================

global_coord = []
for i in range(len(coords)):
    coordinates = [(coords[i][1][0], coords[i][1][1]), (coords[i][3][0], coords[i][3][1])]
    global_coord.append(coordinates)


# ======================================== Change the Geo_projection of the Coordinated =============================
inProj = Proj("+init=EPSG:4326", preserve_units=True)
outProj = Proj("+init=EPSG:32615", preserve_units=True)

global_coord_conv=[]
for i in range(len(global_coord)):

    gcc1 = transform(inProj, outProj, global_coord[i][0][0], global_coord[i][0][1])
    gcc2 = transform(inProj, outProj, global_coord[i][1][0], global_coord[i][1][1])
    global_coord_conv.append([gcc1,gcc2])


# ======================================== Load the image in GDAL and Get geoTransformation Values ==================

srcImage = gdal.Open(file_path)
geoTrans = srcImage.GetGeoTransform()


def world2Pixel(geoMatrix, x, y):
  ulX = geoMatrix[0]
  ulY = geoMatrix[3]
  xDist = geoMatrix[1]
  yDist = geoMatrix[5]
  rtnX = geoMatrix[2]
  rtnY = geoMatrix[4]
  pixel = int((x - ulX) / xDist)
  line = int((ulY - y) / xDist)
  return (pixel, line)


def Pixel2world(geoMatrix, x, y):
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    yDist = geoMatrix[5]
    coorX = (ulX + (x * xDist))
    coorY = (ulY + (y * yDist))
    return (coorX, coorY)


# ============================ Converting the Global Coordinates into the Pixel Coordinates ==========================

pixel_coord =[]
for i in range(len(global_coord_conv)):
    x,y = world2Pixel(geoTrans, global_coord_conv[i][0][0], global_coord_conv[i][0][1])
    g,h = world2Pixel(geoTrans, global_coord_conv[i][1][0], global_coord_conv[i][1][1])
    pixel_coord.append([(x, y), (g, h)])


# ================================== Iterate the Boxes into the Image Processing ======================================

x1=[]
y1=[]
x2=[]
y2=[]

count = 0
for i in range(len(pixel_coord)):

    crop_img = img[pixel_coord[i][0][1]:pixel_coord[i][1][1], pixel_coord[i][0][0]:pixel_coord[i][1][0]]
    hsv = cv2.cvtColor(crop_img, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, (0, 0, 61), (255, 255, 255))
    imask = mask > 0
    green = np.zeros_like(crop_img, np.uint8)
    green[imask] = crop_img[imask]

    gray = cv2.cvtColor(green, cv2.COLOR_BGR2GRAY)
    ret, threshold = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    kernel = np.ones((5, 5), np.uint8)
    img_dilation = cv2.dilate(threshold, kernel, iterations=1)
    contours, _ = cv2.findContours(img_dilation, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

    if len(contours) > 1:
        c = max(contours, key=cv2.contourArea)
        area_max = cv2.contourArea(c)
        count += 1
        if area_max > 2000:
            wx, wy, ww, wh = cv2.boundingRect(c)
            x1.append(wx + pixel_coord[i][0][0]+4)
            y1.append(wy + pixel_coord[i][0][1]+4)
            x2.append(wx + ww + pixel_coord[i][0][0]-4)
            y2.append(wy + wh + pixel_coord[i][0][1]-4)
        else:
            x1.append(pixel_coord[i][0][0]+6)
            y1.append(pixel_coord[i][0][1]+6)
            x2.append(pixel_coord[i][1][0]-6)
            y2.append(pixel_coord[i][1][1]-6)
    else:
        count += 1
        x1.append(pixel_coord[i][0][0] + 6)
        y1.append(pixel_coord[i][0][1] + 6)
        x2.append(pixel_coord[i][1][0] - 6)
        y2.append(pixel_coord[i][1][1] - 6)

print('number of Image Processed-Boxes generated is', count)

# ======================================== Drawing Boxes on the Source Image =====================================

for i in range(len(x1)):
    cv2.rectangle(source, (x1[i], y1[i]), (x2[i], y2[i]), (0, 255, 0), 2)

# plt.imshow(cv2.cvtColor(source, cv2.COLOR_BGR2RGB)), plt.show()


# ============================== Converting the Pixel Coordinates into the Global Coordinates======================

def set_crs(x, y):
    inProj = Proj(init='epsg:32615')
    outProj = Proj(init='epsg:4326')
    x2, y2 = transform(inProj, outProj, x, y)
    return x2, y2


geo_x = []
geo_y = []
geo_x1 = []
geo_y1 = []
for i in range(len(x1)):
    n, m = Pixel2world(geoTrans, x1[i], y1[i])
    o, p = Pixel2world(geoTrans, x2[i],  y2[i])
    n, m = set_crs(n, m)
    o, p = set_crs(o, p)
    geo_x.append(n)
    geo_y.append(m)
    geo_x1.append(o)
    geo_y1.append(p)


final_coordinates = []
for i in range(len(geo_x)):
    final_coordinates.append([geo_x[i], geo_y[i], geo_x1[i], geo_y1[i]])


kml = simplekml.Kml()
for row in final_coordinates:
    print(row)
    pol = kml.newpolygon(name='Syngenta', outerboundaryis=[(row[0], row[1]),
                                                         (row[2], row[1]),
                                                         (row[2], row[3]),
                                                         (row[0], row[3]),
                                                         (row[0], row[1])])

kml.save('Syngenta_boxes_edit.kml')

print('KML Exported Successfully')

end_time = datetime.now()
print('Total Duration of the Execution is {}'.format(end_time - start_time), "seconds")

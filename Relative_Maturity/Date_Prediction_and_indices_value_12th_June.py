"""Code Summary

This code will read the raster and compute the indices value and return the .csv file with respective ID's and Values.
Also, It can predict the date of immature plots with respect to the linear index values.
"""


from datetime import datetime
start_time = datetime.now()

# ================================== Import Packages ======================================================
from pykml import parser
import simplekml
import cv2
from matplotlib import pyplot as plt
import numpy as np
import gdal
from pyproj import Proj, transform
from skimage import io, img_as_float
from skimage.io import imread
import pandas as pd

from skimage.exposure import cumulative_distribution
from pathlib import Path

cv2.useOptimized()




def set_crs(x, y):
    inProj = Proj(init='epsg:32615')
    outProj = Proj(init='epsg:4326')
    x2, y2 = transform(inProj, outProj, x, y)
    return x2, y2



def world2Pixel(geoMatrix, x, y):
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    yDist = geoMatrix[5]
    rtnX = geoMatrix[2]
    rtnY = geoMatrix[4]
    pixel = int((x - ulX) / xDist)
    line = int((ulY - y) / xDist)
    return (pixel, line)


def Pixel2world(geoMatrix, x, y):
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    yDist = geoMatrix[5]
    coorX = (ulX + (x * xDist))
    coorY = (ulY + (y * yDist))
    return (coorX, coorY)


def cdf(im):
 '''
 computes the CDF of an image im as 2D numpy ndarray
 '''
 c, b = cumulative_distribution(im)
 # pad the beginning and ending pixels and their CDF values
 c = np.insert(c, 0, [0]*b[0])
 c = np.append(c, [1]*(255-b[-1]))
 return c


def hist_matching(c, c_t, im):
 '''
 c: CDF of input image computed with the function cdf()
 c_t: CDF of template image computed with the function cdf()
 im: input image as 2D numpy ndarray
 returns the modified pixel values
 '''
 pixels = np.arange(256)
 # find closest pixel-matches corresponding to the CDF of the input image, given the value of the CDF H of
 # the template image at the corresponding pixels, s.t. c_t = H(pixels) <=> pixels = H-1(c_t)
 new_pixels = np.interp(c, c_t, pixels)
 im = (np.reshape(new_pixels[im.ravel()], im.shape)).astype(np.uint8)
 return im

# =================================== Reading the Image and KML ==========================================

file_paths=['/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/Year_2019/SYNGENTA/SOY_Relative_Maturity/Conventional/Tiff/CONV_FLYS/CONV_082218.tif',
'/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/Year_2019/SYNGENTA/SOY_Relative_Maturity/Conventional/Tiff/CONV_FLYS/CONV_090818.tif',
'/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/Year_2019/SYNGENTA/SOY_Relative_Maturity/Conventional/Tiff/CONV_FLYS/CONV_091118.tif',
'/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/Year_2019/SYNGENTA/SOY_Relative_Maturity/Conventional/Tiff/CONV_FLYS/CONV_091418.tif',
'/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/Year_2019/SYNGENTA/SOY_Relative_Maturity/Conventional/Tiff/CONV_FLYS/CONV_091718.tif',
'/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/Year_2019/SYNGENTA/SOY_Relative_Maturity/Conventional/Tiff/CONV_FLYS/CONV_091918.tif',
'/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/Year_2019/SYNGENTA/SOY_Relative_Maturity/Conventional/Tiff/CONV_FLYS/CONV_092218.tif',
'/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/Year_2019/SYNGENTA/SOY_Relative_Maturity/Conventional/Tiff/CONV_FLYS/CONV_092518.tif',
'/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/Year_2019/SYNGENTA/SOY_Relative_Maturity/Conventional/Tiff/CONV_FLYS/CONV_092618.tif',
'/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/Year_2019/SYNGENTA/SOY_Relative_Maturity/Conventional/Tiff/CONV_FLYS/CONVTEST_092818.tif',
'/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/Year_2019/SYNGENTA/SOY_Relative_Maturity/Conventional/Tiff/CONV_FLYS/CONVTEST_100218.tif',
'/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/Year_2019/SYNGENTA/SOY_Relative_Maturity/Conventional/Tiff/CONV_FLYS/CONV_100418.tif',
]
date_nos =['08/09/18',
           '11/09/18',
           '14/09/18',
           '17/09/18',
           '19/09/18',
           '22/09/18',
           '25/09/18',
           '26/09/18',
           '28/09/18',
           '02/10/18',
           '04/10/18'
           ]


print('Pixel Value Iteration on run')

for it in range(len(file_paths)):

    img = imread(file_paths[it])


    # ref_path='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/Year_2019/SYNGENTA/SOY_Relative_Maturity/Conventional/Tiff/CONV_FLYS/CONV_082218.tif'
    # ref_img = imread(ref_path)




    # if it == 0:
    kml_file = "/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/Year_2019/SYNGENTA/SOY_Relative_Maturity/Conventional/3rdjune_Green_band_Analysis_with_color_correction/gdivrb/0.55/next_plot/CONV_100418_rem.kml"
    # else:
    #     v = Path(file_paths[it-1]).name
    #     file = v.replace(".", " ").split()[0]
    #     kml_file = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/Year_2019/SYNGENTA/SOY_Relative_Maturity/Conventional/3rdjune_Green_band_Analysis_with_color_correction/11th_June_VARI_Condition/Source_KMLS/'+file+'_rem.kml'

    f = open(kml_file, "r")
    docs = parser.parse(f)
    try:
        doc = docs.getroot().Document.Folder
    except:
        doc = docs.getroot().Document
    # print('number of features in KML is', len(doc.Placemark))

    # ===================================== Load KML and append the Data in variable ============================

    ID = []
    coords = []
    for place in doc.Placemark:
        x = str(place.Polygon.outerBoundaryIs.LinearRing.coordinates)
        if '\n' not in x:
            x = x.replace(' ', ',')
        else:
            x = x.replace('\n', ',')
            x = x.replace(' ', '')
        x = x.strip().split("\n")
        x = [i.split(',') for i in x]
        x = x[0]
        # for i in x:
        #     i = i.replace(' ', ',')
        #     i = i.split(",")
        try:
            x.remove('')
        except:
            pass
        # print(x)
        co = [[float(x[0]), float(x[1])], [float(x[3]), float(x[4])],
              [float(x[6]), float(x[7])], [float(x[9]), float(x[10])]]
        coords.append(co)
        # y = str(place.ExtendedData.SchemaData.SimpleData)
        y = str(place.description)
        # print(y)
        # if it == 0:
        i = y.split("=")
        des = i[-1]
        des = des.split('<')
        des = des[0]
        des =des.strip()
        # else:
        #     des= y.strip()
        # print(des)
        # des = des.strip()
        ID.append(des)


    # print('number of Coordinates Extracted from KML is', len(coords))



    # ======================================== Change the Geo_projection of the Coordinated ==========================
    inProj = Proj("+init=EPSG:4326", preserve_units=True)
    outProj = Proj("+init=EPSG:32615", preserve_units=True)

    global_coord_conv=[]
    for i in range(len(coords)):

        gcc1 = transform(inProj, outProj, coords[i][0][0], coords[i][0][1])
        gcc2 = transform(inProj, outProj, coords[i][1][0], coords[i][1][1])
        gcc3 = transform(inProj, outProj, coords[i][2][0], coords[i][2][1])
        gcc4 = transform(inProj, outProj, coords[i][3][0], coords[i][3][1])
        global_coord_conv.append([gcc1, gcc2, gcc3, gcc4])


    # ======================================== Load the image in GDAL and Get geoTransformation Values ===============

    srcImage = gdal.Open(file_paths[it])
    geoTrans = srcImage.GetGeoTransform()
    # ============================ Converting the Global Coordinates into the Pixel Coordinates =======================

    pixel_coord =[]
    for i in range(len(global_coord_conv)):
        x,y = world2Pixel(geoTrans, global_coord_conv[i][0][0], global_coord_conv[i][0][1])
        g,h = world2Pixel(geoTrans, global_coord_conv[i][1][0], global_coord_conv[i][1][1])
        x1, y1 = world2Pixel(geoTrans, global_coord_conv[i][2][0], global_coord_conv[i][2][1])
        g1, h1 = world2Pixel(geoTrans, global_coord_conv[i][3][0], global_coord_conv[i][3][1])
        pixel_coord.append([(x, y), (g, h),(x1, y1), (g1, h1)])


    # ================================== Iterate the Boxes into the Image Processing ==================================

    pixel_value = []
    ID_Value = []
    itervalue = []

    mature_pixal_coord=[]
    for i in range(len(pixel_coord)):
        # "Play Area is Here"
        try:
            x = [pixel_coord[i][0][0], pixel_coord[i][1][0], pixel_coord[i][2][0], pixel_coord[i][3][0]]
            y = [pixel_coord[i][0][1], pixel_coord[i][1][1], pixel_coord[i][2][1], pixel_coord[i][3][1]]
            x1, y1 = (min(x), min(y))
            x2, y2 = (max(x), max(y))
            crop_img = img[y1:y2, x1:x2]

            red = crop_img[:, :, 0]
            green = crop_img[:, :, 1]
            blue = crop_img[:, :, 2]

            gdivrb= np.mean(green)/(np.mean(red)+np.mean(blue))

            red_avg = np.mean(red)
            green_avg = np.mean(green)
            blue_avg = np.mean(blue)
            pixel_value.append(gdivrb)

# Iteration 3 - SOY RM Prediction Methodology - 11June2019
            # if (red_avg > green_avg) and (green_avg + red_avg - blue_avg) > 0):
# Iteration 4 - SOY RM Prediction Methodology - 11June2019
#             if 2*green_avg < (red_avg+blue_avg) and red_avg > green_avg and (green_avg + red_avg - blue_avg) > 0:

# Iteration 5 - SOY RM Prediction Methodology - 11June2019
#             if 20*green_avg < 11*(red_avg+blue_avg):
            if gdivrb < 0.55:
                mature_pixal_coord.append(pixel_coord[i])
                itervalue.append(i)
                ID_Value.append(ID[i])


        except:
            print('exception')



    v = Path(file_paths[it]).name
    v = v.replace(' ','_')
    file = v.replace(".", " ").split()[0]

    B = mature_pixal_coord
#     # ==================================== CSV =================================================
#
    if it == 0:
        df = pd.DataFrame(pixel_value, columns=[file])
        df.to_csv('/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/Year_2019/SYNGENTA/SOY_Relative_Maturity/Conventional/3rdjune_Green_band_Analysis_with_color_correction/12th_June_Date_prediction/CSV/values.csv',index= False)
    else:
        df = pd.read_csv("/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/Year_2019/SYNGENTA/SOY_Relative_Maturity/Conventional/3rdjune_Green_band_Analysis_with_color_correction/12th_June_Date_prediction/CSV/values.csv")
        df[file] = pixel_value
        df.to_csv('/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/Year_2019/SYNGENTA/SOY_Relative_Maturity/Conventional/3rdjune_Green_band_Analysis_with_color_correction/12th_June_Date_prediction/CSV/values.csv',index= False)


# ======================================== Date Prediction =============================================

df = pd.read_csv("/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/Year_2019/SYNGENTA/SOY_Relative_Maturity/Conventional/3rdjune_Green_band_Analysis_with_color_correction/12th_June_Date_prediction/CSV/values.csv")

predicted = []

for i in range(len(df)):

    index_value = df.iloc[i]
    index_value_list = index_value.to_list()
    # plt.plot(range(len(p)),p)


    from sklearn.linear_model import LinearRegression
    from sklearn.metrics import mean_squared_error, r2_score

    x= index_value_list
    y = [1,4,6]
    x = np.reshape(x, (1, -1)).T
    y = np.reshape(y, (1, -1)).T

    X=x
    Y=y
    regression_model = LinearRegression()
    regression_model.fit(x, y)
    y_predicted = regression_model.predict(x)
    # rmse = mean_squared_error(y, y_predicted)
    # r2 = r2_score(y, y_predicted)
    # print('Slope:' ,regression_model.coef_)
    # print('Intercept:', regression_model.intercept_)
    # print('Root mean squared error: ', rmse)
    # print('R2 score: ', r2)
    #
    #
    # plt.scatter(y, x, s=10)
    # plt.xlabel('x')
    # plt.ylabel('y')
    # # predicted values
    # plt.plot(y_predicted,x, color='r')
    # plt.show()
    #
    pred = regression_model.predict([[0.55]])
    pred = round(pred[0][0])
    dates = {7: '05/10/18', 8: '06/10/18', 9: '07/10/18', 10: '08/10/18', 11: '09/10/18'}
    try:
        pred_date = dates[pred]
    except:
        pred_date = '05/10/18'
        print(pred)
    predicted.append(pred_date)


df = pd.DataFrame(predicted, columns=['Date'])
df['ID'] = ID
df.to_csv('/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/Year_2019/SYNGENTA/SOY_Relative_Maturity/Conventional/3rdjune_Green_band_Analysis_with_color_correction/12th_June_Date_prediction/rem_ID.csv',index= False)



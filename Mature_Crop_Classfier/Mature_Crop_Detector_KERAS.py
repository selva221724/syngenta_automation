from datetime import datetime
start_time = datetime.now()

# ================================== Import Packages ======================================================
from pykml import parser
import simplekml
import cv2
import numpy as np
import gdal
from pyproj import Proj, transform
from skimage import io, img_as_float
from skimage.io import imread
from matplotlib import pyplot as plt

cv2.useOptimized()





# =================================== Reading the Image and KML ==========================================

# file_path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_082218.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_090818.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_091118.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_091418.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_091718.tif'
# file_path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_091918.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_092218.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_092518.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_092618.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONVTEST_092818.tif'
file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONVTEST_100218.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_100418.tif'




output_path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/R&D/Mature_Crop_Classfier/Resnet_classifier/ouput'
#===================================== Load Model ==========================================================
from keras.models import model_from_json

#
# from keras.models import Sequential
# from keras_contrib.losses  import crf_loss
# from keras_contrib.metrics import crf_viterbi_accuracy
# from keras contrib.layers import crf



# To load the model
# custom_objects={'CRF'               : CRF,
#               'crf_loss'            : crf_loss,
#               'crf_viterbi_accuracy': crf_viterbi_accuracy}

# To load a persisted model that uses the CRF layer
# model1 = load_model("/home/abc/my_model_01.hdf5", custom_objects = custom_objects)


# load json and create model

json_file = open('/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/R&D/Mature_Crop_Classfier/Resnet_classifier/19thMay/model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
loaded_model.load_weights("/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/R&D/Mature_Crop_Classfier/Resnet_classifier/19thMay/Xception_saved-model-01-1.00.hdf5")
print("Loaded model from disk")



import numpy as np
from keras.preprocessing import image

import numpy as np
from keras.preprocessing import image
def detection(img):
    test_image = np.expand_dims(img, axis=0)
    result = loaded_model.predict(test_image)
    # training_set.class_indices
    # if result[0][0] >= 0.5:
    #     pre = 'green'
    # else:
    #     pre = 'mature'
    # return pre

    if result[0][0] >= 0.5:
        pre = 'mature'
    else:
        pre = 'green'
    return pre


img = imread(file_path)
img = img[:,:,:3]


kml_file = "/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/grid/grid.kml"
f = open(kml_file, "r")
docs = parser.parse(f)
doc = docs.getroot().Document.Folder
print('number of features in KML is', len(doc.Placemark))


# ===================================== Load KML and append the Data in variable ============================

# ID = []
coords = []
for place in doc.Placemark:
    x = str(place.Polygon.outerBoundaryIs.LinearRing.coordinates)
    x = x.strip().split("\n")
    x = [i.strip() for i in x]
    m = 1
    for i in x:
        i = i.replace(' ', ',')
        i = i.split(",")
        co = [[float(i[0]), float(i[1])], [float(i[3]), float(i[4])], [float(i[6]), float(i[7])], [float(i[9]), float(i[10])]]
        coords.append(co)

    # y = str(place.ExtendedData.SchemaData.SimpleData)
    # ID.append(y)


print('number of Coordinates Extracted from KML is',len(coords))


# ======================================= Store the coordinates is the List ===================================

global_coord = []
for i in range(len(coords)):
    coordinates = [(coords[i][2][0], coords[i][2][1]), (coords[i][0][0], coords[i][0][1])]
    global_coord.append(coordinates)


# # ======================================== Change the Geo_projection of the Coordinated =============================
inProj = Proj(init="epsg:4326")
outProj = Proj(init="epsg:32615")

global_coord_conv=[]
for i in range(len(global_coord)):

    gcc1 = transform(inProj, outProj, global_coord[i][0][0], global_coord[i][0][1])
    gcc2 = transform(inProj, outProj, global_coord[i][1][0], global_coord[i][1][1])
    global_coord_conv.append([gcc1,gcc2])

# ======================================== Load the image in GDAL and Get geoTransformation Values ==================

srcImage = gdal.Open(file_path)
geoTrans = srcImage.GetGeoTransform()


def world2Pixel(geoMatrix, x, y):
  ulX = geoMatrix[0]
  ulY = geoMatrix[3]
  xDist = geoMatrix[1]
  yDist = geoMatrix[5]
  rtnX = geoMatrix[2]
  rtnY = geoMatrix[4]
  pixel = int((x - ulX) / xDist)
  line = int((ulY - y) / xDist)
  return (pixel, line)


def Pixel2world(geoMatrix, x, y):
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    yDist = geoMatrix[5]
    coorX = (ulX + (x * xDist))
    coorY = (ulY + (y * yDist))
    return (coorX, coorY)


# ============================ Converting the Global Coordinates into the Pixel Coordinates ==========================

pixel_coord =[]
for i in range(len(global_coord_conv)):
    x,y = world2Pixel(geoTrans, global_coord_conv[i][0][0], global_coord_conv[i][0][1])
    g,h = world2Pixel(geoTrans, global_coord_conv[i][1][0], global_coord_conv[i][1][1])
    pixel_coord.append([(x, y), (g, h)])


# ================================== Iterate the Boxes into the Image Processing ======================================

print('Model is predicting the Plots into mature and green')


iter_el = []

x1=[]
y1=[]
x2=[]
y2=[]
x = []

for i in range(len(pixel_coord)):

    crop_img = img[pixel_coord[i][1][1]:pixel_coord[i][0][1], pixel_coord[i][1][0]:pixel_coord[i][0][0]]
    crop_img = cv2.resize(crop_img, (40,150))
    # test_image = np.expand_dims(crop_img, axis=0)
    x.append(crop_img)






    # pre = detection(crop_img)
    #
    # if pre == 'mature' :
    #     x1.append(pixel_coord[i][0][0])
    #     y1.append(pixel_coord[i][0][1])
    #     x2.append(pixel_coord[i][1][0])
    #     y2.append(pixel_coord[i][1][1])
    #     iter_el.append(i)
    #

def chunks(l, n):
    for i in range(0, len(l), n):
    # Create an index range for l of n items:
        yield l[i:i+n]



img_batch = 450

y = x[:img_batch]
y = np.array(y)

def prediction(y):
    el = []
    result = loaded_model.predict_on_batch(y)
    r = result[:, 0]
    for i in range(len(r)):
        # if r[i] >= 0.5:
        #     pre = 'green'
        # else:
        #     pre = 'mature'
        #     el.append(i)

        if r[i] >= 0.5:
            pre = 'mature'
            el.append(i)
        else:
            pre = 'green'


    return el


X = list(chunks(x, img_batch))

main_list = []
item = 0
for i in range(len(X)):
    y= np.array(X[i])
    d = prediction(y)
    for j in range(len(d)):
        main_list.append(item+d[j])
    item+=img_batch

# ID_Value_rem =  [i for j, i in enumerate(pixel_value) if j not in main_list]
from operator import itemgetter

grid_id = itemgetter(*main_list)(pixel_coord)




#
# x=[]
# for i in range(0,16800):
#     x.append(i)
#
# def sd(a):
#     sum=0
#     d =[]
#     for i in range(len(a)):
#         sum = sum + a[i]
#         d.append(a[i])
#     return d
#


# X = list(chunks(x, 400))
#
# main_list = []
# item = 0
# for i in range(len(X)):
#     d = sd(X[i])
#     for j in range(len(d)):
#         main_list.append(item+d[i])
#     item+=400




# from operator import itemgetter
#
# grid_id = itemgetter(*iter_el)(ID)
#
# from pathlib import Path
# v = Path(file_path).name
# v = v.replace(' ','_')
# file = v.replace(".", " ").split()[0]
#
# import pandas as pd
# #
# # data = pd.DataFrame({file:grid_id})
# # data.to_csv("RM_ID.csv",index=False)
#
#
# #
# dataset = pd.read_csv("RM_ID.csv")
# g= pd.DataFrame({file:grid_id})
# dataset = pd.concat([dataset,g], axis=1)
# dataset.to_csv("RM_ID.csv",index=False)
#
# print('Mature Count is '+ str(len(x1)))
## ======================================== Drawing Boxes on the Source Image =====================================
#
# for i in range(len(x1)):
#     cv2.rectangle(source, (x1[i], y1[i]), (x2[i], y2[i]), (0, 255, 0), 2)
#
# plt.imshow(cv2.cvtColor(source, cv2.COLOR_BGR2RGB)), plt.show()
#
#
# ============================== Converting the Pixel Coordinates into the Global Coordinates======================

def set_crs(x, y):
    inProj = Proj(init='epsg:32615')
    outProj = Proj(init='epsg:4326')
    x2, y2 = transform(inProj, outProj, x, y)
    return x2, y2


geo_x = []
geo_y = []
geo_x1 = []
geo_y1 = []
for i in range(len(grid_id)):
    n, m = Pixel2world(geoTrans, grid_id[i][0][0], grid_id[i][0][1])
    o, p = Pixel2world(geoTrans, grid_id[i][1][0],  grid_id[i][1][1])
    n, m = set_crs(n, m)
    o, p = set_crs(o, p)
    geo_x.append(n)
    geo_y.append(m)
    geo_x1.append(o)
    geo_y1.append(p)


final_coordinates = []
for i in range(len(geo_x)):
    final_coordinates.append([geo_x[i], geo_y[i], geo_x1[i], geo_y1[i]])

print('Mature Count is '+ str(len(final_coordinates)))

kml = simplekml.Kml()
for row in final_coordinates:
    # print(row)
    pol = kml.newpolygon(outerboundaryis=[(row[0], row[1]),
                                                         (row[2], row[1]),
                                                         (row[2], row[3]),
                                                         (row[0], row[3]),
                                                         (row[0], row[1])])

from pathlib import Path
v = Path(file_path).name
file = v.replace(".", " ").split()[0]
kml.save(output_path+'/'+file+'.kml')



print(len(final_coordinates))

print('KML Exported Successfully')

end_time = datetime.now()
print('Total Duration of the Execution is {}'.format(end_time - start_time), "seconds")

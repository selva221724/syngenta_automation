from keras.models import Sequential
from keras.layers import Convolution2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
import os
import tensorflow as tf

from keras.callbacks import ModelCheckpoint

from keras.applications.mobilenet import MobileNet, preprocess_input

HEIGHT = 128
WIDTH = 128
BATCH_SIZE = 100
NUM_EPOCHS = 5
num_train_images = 33468
num_validation_images = 14344

base_model = MobileNet(weights='imagenet',
                      include_top=False,
                      input_shape=(HEIGHT, WIDTH, 3))



from keras.preprocessing.image import ImageDataGenerator

TRAIN_DIR = "/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/R&D/Mature_Crop_Classfier/Dataset/9th_May/Classifier/training_set"




train_datagen =  ImageDataGenerator(
      preprocessing_function=preprocess_input,
      rotation_range=90,
      horizontal_flip=True,
      vertical_flip=True
    )

test_datagen = ImageDataGenerator(rescale=1./225)




train_generator = train_datagen.flow_from_directory(TRAIN_DIR,
                                                    target_size=(HEIGHT, WIDTH),
                                                    batch_size=BATCH_SIZE)




validation_set = test_datagen.flow_from_directory('/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/R&D/Mature_Crop_Classfier/Dataset/9th_May/Classifier/validation_set',
                                            target_size=(HEIGHT, WIDTH),
                                            batch_size=BATCH_SIZE,)


from keras.layers import Dense, Activation, Flatten, Dropout
from keras.models import Sequential, Model


def build_finetune_model(base_model, dropout, fc_layers, num_classes):
    for layer in base_model.layers:
        layer.trainable = False

    x = base_model.output
    x = Flatten()(x)
    for fc in fc_layers:
        # New FC layer, random init
        x = Dense(fc, activation='relu')(x)
        x = Dropout(dropout)(x)

    # New softmax layer
    predictions = Dense(num_classes, activation='softmax')(x)

    finetune_model = Model(inputs=base_model.input, outputs=predictions)

    return finetune_model


class_list = ["Green_plots", "Mature_plots"]
FC_LAYERS = [1024, 1024]
dropout = 0.2

finetune_model = build_finetune_model(base_model,
                                      dropout=dropout,
                                      fc_layers=FC_LAYERS,
                                      num_classes=len(class_list))


from keras.optimizers import SGD, Adam


# num_train_images = 400

adam = Adam(lr=0.00001)
finetune_model.compile(adam, loss='categorical_crossentropy', metrics=['accuracy'])

# filepath="/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/R&D/Mature_Crop_Classfier/Resnet_classifier/checkpoints/" + "ResNet50" + "_model_weights.h5"

filepath="/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/R&D/Mature_Crop_Classfier/Resnet_classifier/19thMay/" + \
         "Xception_"+"saved-model-{epoch:02d}-{val_acc:.2f}.hdf5"
checkpoint = ModelCheckpoint(filepath, monitor=["acc","val_acc"], verbose=1, mode='max',save_best_only=False)
callbacks_list = [checkpoint]

history = finetune_model.fit_generator(train_generator, epochs=NUM_EPOCHS, workers=8,
                                       steps_per_epoch=num_train_images // BATCH_SIZE,
                                       shuffle=True, callbacks=callbacks_list,
                                       validation_data=validation_set,
                                       validation_steps=num_validation_images // BATCH_SIZE)



#
# # Plot the training and validation loss + accuracy
# # def plot_training(history):
# #     acc = history.history['acc']
# #     val_acc = history.history['val_acc']
# #     loss = history.history['loss']
# #     val_loss = history.history['val_loss']
# #     epochs = range(len(acc))
# #
# #     plt.plot(epochs, acc, 'r.')
# #     plt.plot(epochs, val_acc, 'r')
# #     plt.title('Training and validation accuracy')
# #
# #     # plt.figure()
# #     # plt.plot(epochs, loss, 'r.')
# #     # plt.plot(epochs, val_loss, 'r-')
# #     # plt.title('Training and validation loss')
# #     plt.show()
# #
# #     plt.savefig('acc_vs_epochs.png')
# #
# #
# # plot_training(history)
#
model_json = finetune_model.to_json()
with open("/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/R&D/Mature_Crop_Classfier/Resnet_classifier/19thMay/model.json", "w") as json_file:
    json_file.write(model_json)

#
# from keras.models import model_from_json
# json_file = open('/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/R&D/Mature_Crop_Classfier/Resnet_classifier/checkpoints/model.json', 'r')
# loaded_model_json = json_file.read()
# json_file.close()
# loaded_model = model_from_json(loaded_model_json)
# loaded_model.load_weights("/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/R&D/Mature_Crop_Classfier/Resnet_classifier/checkpoints/ResNet50_model_weights.h5")
#
#
# import numpy as np
# from keras.preprocessing import image
# def detection(img):
#     test_image = image.load_img(img,target_size=(300,300))
#     test_image = image.img_to_array(test_image)
#     test_image = np.expand_dims(test_image, axis=0)
#     result = loaded_model.predict(test_image)
#     # training_set.class_indices
#     if result[0][0] >= 0.5:
#         pre = 'green'
#     else:
#         pre = 'mature'
#     return print(pre)
#
# detection('/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/R&D/Mature_Crop_Classfier/pred/2.tif')


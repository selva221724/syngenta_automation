import numpy
import os
import shutil
from pathlib import Path

input_path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/R&D/Mature_Crop_Classfier/Dataset/9th_May/Mature_plots'

OUT_PUT_PATH = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/R&D/Mature_Crop_Classfier/Dataset/9th_May/Classifier'

path =[]
for root, directories, filenames in os.walk(input_path):
    for filename in filenames:
        path.append(os.path.join(root,filename))

def match_remover(master,slave):
    match = []
    for k in slave:
        for v in master:
            if k == v:
                match.append(k)

    for h in range(len(match)):
        master.remove(match[h])

    return master

length = len(path)

import random
random.shuffle(path)


train_percentage  = round((length/100)*60)
train = path[:train_percentage]
match_remover(path,train)


test_percentage  = round((length/100)*20)
test = path[:test_percentage]
match_remover(path,test)


validation = path

print("The Training data set size is",len(train))
print("The Test data set size is",len(test))
print("The Validation data set size is",len(validation))

from pathlib import Path
name = Path(input_path).name


if not os.path.exists(OUT_PUT_PATH + '/' + 'training_set'):
    os.makedirs(OUT_PUT_PATH + '/' + 'training_set')

if not os.path.exists(OUT_PUT_PATH + '/' + 'test_set'):
    os.makedirs(OUT_PUT_PATH + '/' + 'test_set')

if not os.path.exists(OUT_PUT_PATH + '/' + 'validation_set'):
    os.makedirs(OUT_PUT_PATH + '/' + 'validation_set')

# ========================= Train_Set =============================================
if not os.path.exists(OUT_PUT_PATH + '/' + 'training_set/'+name):
    os.makedirs(OUT_PUT_PATH + '/' + 'training_set/'+name)

for i in range(len(train)):
    v = Path(train[i]).name
    shutil.copy(train[i],OUT_PUT_PATH + '/' + 'training_set/'+name+'/'+v)

print('The Train set has been created')

# ========================= Test_Set =============================================
if not os.path.exists(OUT_PUT_PATH + '/' + 'test_set/'+name):
    os.makedirs(OUT_PUT_PATH + '/' + 'test_set/'+name)

for i in range(len(test)):
    v = Path(test[i]).name
    shutil.copy(test[i],OUT_PUT_PATH + '/' + 'test_set/'+name+'/'+v)
print('The Test set has been created')

# ========================= Validation_Set =============================================
if not os.path.exists(OUT_PUT_PATH + '/' + 'validation_set/'+name):
    os.makedirs(OUT_PUT_PATH + '/' + 'validation_set/'+name)

for i in range(len(validation)):
    v = Path(validation[i]).name
    shutil.copy(validation[i],OUT_PUT_PATH + '/' + 'validation_set/'+name+'/'+v)

print('The Validation set has been created')


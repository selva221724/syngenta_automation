from datetime import datetime
start_time = datetime.now()

# ================================== Import Packages ======================================================
from pykml import parser
import simplekml
import cv2
from matplotlib import pyplot as plt
import numpy as np
import gdal
from pyproj import Proj, transform
from skimage import io, img_as_float
from skimage.io import imread
from matplotlib import pyplot as plt

cv2.useOptimized()





# =================================== Reading the Image and KML ==========================================

# file_path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_082218.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_090818.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_091118.tif'
file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_091418.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_091718.tif'
# file_path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_091918.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_092218.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_092518.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_092618.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONVTEST_092818.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONVTEST_100218.tif'
# file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/Relative_Maturity/Conventional/Datasets_tiff/CONV_100418.tif'




img = imread(file_path)
source = img


kml_file = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/R&D/Mature_Crop_Classfier/Data_Collection/green/091418_5507_Crops.kml'

f = open(kml_file, "r")
docs = parser.parse(f)
try:

    doc = docs.getroot().Document
    print('Number of features in KML is', len(doc.Placemark))
except:
    try:
        doc = docs.getroot().Document.Folder
        print('Number of features in KML is', len(doc.Placemark))
    except:
        pass

print('number of features in KML is', len(doc.Placemark))


# ===================================== Load KML and append the Data in variable ============================

# ID = []
coords = []
for place in doc.Placemark:
    x = str(place.Polygon.outerBoundaryIs.LinearRing.coordinates)
    x = x.strip().split("\n")
    x = [i.strip() for i in x]
    m = 1
    for i in x:
        i = i.replace(' ', ',')
        i = i.split(",")
        co = [[float(i[0]), float(i[1])], [float(i[3]), float(i[4])], [float(i[6]), float(i[7])], [float(i[9]), float(i[10])]]
        coords.append(co)

    # y = str(place.ExtendedData.SchemaData.SimpleData)
    # ID.append(y)


print('number of Coordinates Extracted from KML is',len(coords))


# ======================================= Store the coordinates is the List ===================================

global_coord = []
for i in range(len(coords)):
    coordinates = [(coords[i][2][0], coords[i][2][1]), (coords[i][0][0], coords[i][0][1])]
    global_coord.append(coordinates)


# ======================================== Change the Geo_projection of the Coordinated =============================
inProj = Proj("+init=EPSG:4326", preserve_units=True)
outProj = Proj("+init=EPSG:32615", preserve_units=True)

global_coord_conv=[]
for i in range(len(global_coord)):

    gcc1 = transform(inProj, outProj, global_coord[i][0][0], global_coord[i][0][1])
    gcc2 = transform(inProj, outProj, global_coord[i][1][0], global_coord[i][1][1])
    global_coord_conv.append([gcc1,gcc2])


# ======================================== Load the image in GDAL and Get geoTransformation Values ==================

srcImage = gdal.Open(file_path)
geoTrans = srcImage.GetGeoTransform()


def world2Pixel(geoMatrix, x, y):
  ulX = geoMatrix[0]
  ulY = geoMatrix[3]
  xDist = geoMatrix[1]
  yDist = geoMatrix[5]
  rtnX = geoMatrix[2]
  rtnY = geoMatrix[4]
  pixel = int((x - ulX) / xDist)
  line = int((ulY - y) / xDist)
  return (pixel, line)


def Pixel2world(geoMatrix, x, y):
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    yDist = geoMatrix[5]
    coorX = (ulX + (x * xDist))
    coorY = (ulY + (y * yDist))
    return (coorX, coorY)


# ============================ Converting the Global Coordinates into the Pixel Coordinates ==========================

pixel_coord =[]
for i in range(len(global_coord_conv)):
    x,y = world2Pixel(geoTrans, global_coord_conv[i][0][0], global_coord_conv[i][0][1])
    g,h = world2Pixel(geoTrans, global_coord_conv[i][1][0], global_coord_conv[i][1][1])
    pixel_coord.append([(x, y), (g, h)])


# ================================== Iterate the Boxes into the Image Processing ======================================
#
pixel_value = []
iter_el = []

X=[]
from pathlib import Path
v = Path(file_path).name

for i in range(len(pixel_coord)):

    x = [pixel_coord[i][0][0], pixel_coord[i][1][0]]
    y = [pixel_coord[i][0][1], pixel_coord[i][1][1]]
    x1,y1  = (min(x), min(y))
    x2, y2 = (max(x), max(y))
    crop_img = img[y1:y2 , x1:x2]
    # gray = cv2.cvtColor(crop_img, cv2.COLOR_BGR2GRAY)
    # plt.hist(gray.ravel(), 256, [50, 150]);
    # plt.show()
    # band,_,_ =gray.ravel(), 256, [50, 150]
    # theMean = np.mean(gray)
    # pixel_value.append(theMean)
    rgb  = cv2.cvtColor(crop_img, cv2.COLOR_BGR2RGB)
    cv2.imwrite(
        '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/R&D/Mature_Crop_Classfier/Dataset/9th_May/Green_plots/'+v+'%d.jpg' % i,
        rgb)


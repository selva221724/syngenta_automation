from keras.models import Sequential
from keras.layers import Convolution2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
import os
import tensorflow as tf


classifier = Sequential()

classifier.add(Convolution2D(32,3,3,input_shape=(64,64,3),activation='relu'))

classifier.add(MaxPooling2D(pool_size=(2,2)))

classifier.add(Flatten())

classifier.add(Dense(output_dim = 128 ,activation='relu'))
classifier.add(Dense(output_dim= 1, activation= 'sigmoid'))

classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy',metrics= ['accuracy'])

from keras.preprocessing.image import ImageDataGenerator

train_datagen = ImageDataGenerator(rescale=1./255,
                                   shear_range=.2,
                                   zoom_range=.2,
                                   horizontal_flip=True)

test_datagen = ImageDataGenerator(rescale=1./225)

training_set = train_datagen.flow_from_directory('/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/R&D/Mature_Crop_Classfier/Dataset/091118_091418_092218_data/training_set',
                                                 target_size=(64,64),
                                                 batch_size=32,
                                                 class_mode='binary')

test_set = test_datagen.flow_from_directory('/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/R&D/Mature_Crop_Classfier/Dataset/091118_091418_092218_data/test_set',
                                            target_size=(64, 64),
                                            batch_size=32,
                                            class_mode='binary')
#
# from IPython.display import display
# from PIL import Image


classifier.fit_generator(training_set,
                         steps_per_epoch=6000,
                         epochs=5,
                         validation_data=test_set,
                         validation_steps=600)



# serialize model to JSON
model_json = classifier.to_json()
with open("/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/R&D/Mature_Crop_Classfier/Saved_Model/12884_Plots/model.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
classifier.save_weights("/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/R&D/Mature_Crop_Classfier/Saved_Model/12884_Plots/model.h5")
print("Saved model to disk")




from keras.models import model_from_json


# load json and create model
json_file = open('/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/R&D/Mature_Crop_Classfier/Saved_Model/2000_Plots/model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# load weights into new model
loaded_model.load_weights("/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/R&D/Mature_Crop_Classfier/Saved_Model/2000_Plots/model.h5")
print("Loaded model from disk")


import numpy as np
from keras.preprocessing import image
def detection(img):
    test_image = image.load_img(img,target_size=(64,64))
    test_image = image.img_to_array(test_image)
    test_image = np.expand_dims(test_image, axis=0)
    result = loaded_model.predict(test_image)
    # training_set.class_indices
    if result[0][0] >= 0.5:
        pre = 'mature'
    else:
        pre = 'green'

    return print(pre) , print(result)

detection('/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Tamil/SYNGENTA/R&D/Mature_Crop_Classfier/pred/2.tif')
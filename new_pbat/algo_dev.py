import geo
from pykml import parser
import re
import numpy as np
from matplotlib import pyplot as plt
import simplekml
from shapely.geometry import Point
import pyproj
from pyproj import Proj, transform
from osgeo import gdal
import pbat
from pandas import DataFrame

kml_path = '/mnt/dash/Alpha_Share/syngenta/tamil/logical_operations/brookings/test.kml'
tif_path = '/mnt/dash/Alpha_Share/syngenta/Syngenta_Updated_4341_Brookings/Syngenta_Updated_4341_Brookings_LIMT_transparent_mosaic_group1.tif'
# rangerow = 95 #mitchell
rangerow = 5 #mitchell

kml = geo.kread(kml_path)

ds=gdal.Open(tif_path)
prj=ds.GetProjection()
geoTrans = ds.GetGeoTransform()
projection = geo.get_projection(tif_path)

outProj = Proj(init='epsg:'+ projection)
inProj = Proj(init='epsg:4326')

pixel_coords=[]
for i in kml:
    temp=[]
    for j in i:
        x,y = geo.set_crs(j[0],j[1],'4326',projection)
        x,y = geo.world2Pixel(geoTrans,x,y)
        temp.append([x,y])
    pixel_coords.append(temp)


# a = [[5557, 17071], [5521, 17071], [5521, 16720], [5557, 16720], [5557, 17071]]

pixel_coords_m=[]
for i in pixel_coords:
    x = [j[0] for j in i]
    y = [j[1] for j in i]
    xmin,ymin = min(x),min(y)
    xmax,ymax = max(x),max(y)
    pixel_coords_m.append([[xmin,ymin],[xmax,ymin],
                         [xmax,ymax],[xmin,ymax],[xmin,ymin]])


cluster_list = pbat.clustering_horizontal(pixel_coords_m,rangerow)


# ===================================================================================
#
def new_plots(change):
    coords = change
    coords_top = []
    for i in range(len(coords) - 1):
        coords_top.append(coords[i][1])
        coords_top.append(coords[i + 1][0])

    avrg_dist = pbat.avrg_plot_distance_finder(coords_top)

    W = []
    H = []
    for i in coords:
        h, w = pbat.find_h_w(i)
        W.append(w)
        H.append(h)
    avrg_width = W[0]
    avrg_height = H[0]

    aco_len = avrg_dist + avrg_width + avrg_dist  # how much can be plotted

    import math
    new_plots = []
    for i in range(len(coords_top) - 1):
        p1 = coords_top[i]
        p2 = coords_top[i + 1]
        distance = round(math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2)))
        if distance > aco_len:
            no_of_plots = round(distance / aco_len)
            for plot in range(no_of_plots):
                p1 = [p1[0] + avrg_dist * plot, p1[1]]
                new = [
                    p1,
                    [p1[0] + avrg_width, p1[1]],
                    [p1[0] + avrg_width, p1[1] + avrg_height],
                    [p1[0], p1[1] + avrg_height],
                    p1
                ]
                new_plots.append(new)
    return new_plots


pixel_coords_updated =[]

for i in cluster_list:
    change =pbat.magic_plot_horizontal(i)
    for plot in change:
        pixel_coords_updated.append(plot)



global_coords=[]
for i in pixel_coords_updated:
    temp=[]
    for j in i:
        x, y = geo.Pixel2world(geoTrans, j[0],j[1])
        x,y = geo.set_crs(x,y,projection,'4326')
        temp.append([x,y])
    global_coords.append(temp)


geo.kgen(global_coords,'out.kml')


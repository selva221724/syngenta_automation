import cv2
import numpy as np
from matplotlib import pyplot as plt
from skimage.io import imread
from osgeo import gdal
from pyproj import Proj, transform
import simplekml
import math
from shapely.geometry import Polygon
import statistics
import scipy.cluster.hierarchy as hcluster
import numpy as np
from sklearn.cluster import KMeans
import pandas as pd


# ========================================= Clustering ==============================================
def findIntersection(x1, y1, x2, y2, x3, y3, x4, y4):
    px = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / (
            (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))
    py = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / (
            (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))
    return [px, py]


def find_h_w(a):
    temp = []
    for i in range(len(a) - 1):
        p1 = a[i]
        p2 = a[i + 1]
        distance = round(math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2)))
        temp.append(distance)

    width = min(temp)
    height = max(temp)

    return height, width


def outlier_detection(f):
    df_in = pd.DataFrame(f, columns=['a'])

    q1 = df_in['a'].quantile(0.25)
    q3 = df_in['a'].quantile(0.75)
    iqr = q3 - q1  # Interquartile range
    fence_low = q1 - 1.5 * iqr
    fence_high = q3 + 1.5 * iqr
    df_out = df_in.loc[(df_in['a'] > fence_low) & (df_in['a'] < fence_high)]
    if not list(df_out['a']):
        return f
    else:
        return list(df_out['a'])


def avrg_plot_distance_finder(a):
    temp = []
    for i in range(len(a) - 1):
        p1 = a[i]
        p2 = a[i + 1]
        distance = round(math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2)))
        temp.append(distance)

    temp = outlier_detection(temp)
    avrg = statistics.mean(temp)

    return avrg


def plot_distance_finder(a):
    temp = []
    for i in range(len(a) - 1):
        p1 = a[i]
        p2 = a[i + 1]
        distance = round(math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2)))
        temp.append(distance)

    # temp = outlier_detection(temp)
    # avrg = statistics.mean(temp)

    return temp


def clustering_horizontal(pixel_coords_m, number_of_cluster):
    imcentx = [i[0][0] for i in pixel_coords_m]
    imcenty = [i[0][1] for i in pixel_coords_m]

    imcenty = np.array(imcenty)

    kmeans = KMeans(init='k-means++', n_clusters=number_of_cluster, max_iter=1000).fit(imcenty.reshape(-1, 1))
    id_label = kmeans.labels_
    # plt.scatter(imcentx,imcenty,c=kmeans.labels_)
    # plt.show()

    ids = list(id_label)

    cluster_list = []
    for i in range(number_of_cluster):
        temp = []
        for j, k in zip(pixel_coords_m, ids):
            if k == i:
                temp.append(j)
        cluster_list.append(temp)

    return cluster_list


def false_poitive_remover(pixel_coords_m, half_h, half_w):
    updated_plots =[]
    for i in pixel_coords_m:
        h,w = find_h_w(i)
        # print(h)
        if w < (half_w*2)*0.8:
            continue
        elif w > (half_w*2)*1.2:
            continue
        elif h < (half_h*2)*0.9:
            continue
        elif h > (half_h*2)*1.1:
            continue
        else:
            updated_plots.append(i)

    updated_plots = sorted(updated_plots)

    return updated_plots

def width_equalizer(pixel_coords_m, half_w):
    pixel_coords_m = sorted(pixel_coords_m)
    # =================================== Width Adjustment ========================================

    height_updated = []
    for i in pixel_coords_m:
        mid = list(Polygon(i).centroid.coords)[0]
        mid = [round(mid[0]), round(mid[1])]
        change = [
            [mid[0] - half_w, i[0][1]],
            [mid[0] + half_w, i[1][1]],
            [mid[0] + half_w, i[2][1]],
            [mid[0] - half_w, i[3][1]],
            [mid[0] - half_w, i[4][1]]
        ]
        height_updated.append(change)

    return height_updated


def magic_plots_range_to_range(range1, range2):
    updated_plots = []
    for i in range1:
        p1 = Polygon(i)
        h, w = find_h_w(i)
        plot = i

        for j in range2:
            p2 = Polygon(j)

            if p1.intersects(p2):
                p3 = p1.intersection(p2).area
                p1_a = p1.area
                overlap_percet = ((p3 / p1_a) * 100) + 5
                reduce_percent = round((overlap_percet / 100) * h)

                change = [
                    i[0],
                    i[1],
                    [i[2][0], i[2][1] - reduce_percent],
                    [i[3][0], i[3][1] - reduce_percent],
                    i[4]
                ]
                plot = change
                continue

        updated_plots.append(plot)

    return updated_plots


def fill_the_gaps(height_updated, plot_mid_dist, half_w, half_h):
    new_plots = []
    for i in range(len(height_updated) - 1):
        p1 = list(Polygon(height_updated[i]).centroid.coords)[0]
        p2 = list(Polygon(height_updated[i + 1]).centroid.coords)[0]
        point_dist = round(math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2)))
        if point_dist > plot_mid_dist * 1.5:
            no_plots = int((point_dist / plot_mid_dist)+0.5)
            if not no_plots == 1:
                no_plots = no_plots - 1
            if not no_plots == 0:
                point = p1[0]
                new_mids = []
                for plot in range(no_plots):
                    x, y = plot_mid_dist + point, p1[1]
                    try:
                        x1, y1 = findIntersection(p1[0], p1[1], p2[0], p2[1], x, y*-10, x, y*10)
                    except:
                        x1,y1 = x,y
                    point += plot_mid_dist
                    new_mids.append([x, y1])

                for mid in new_mids:
                    change = [
                        [mid[0] - half_w, mid[1] - half_h],
                        [mid[0] + half_w, mid[1] - half_h],
                        [mid[0] + half_w, mid[1] + half_h],
                        [mid[0] - half_w, mid[1] + half_h],
                        [mid[0] - half_w, mid[1] - half_h]
                    ]
                    new_plots.append(change)

    for i in new_plots:
        height_updated.append(i)

    height_updated = sorted(height_updated)

    return height_updated


def Stright_liner(height_updated):
    height_updated = sorted(height_updated)
    start = list(Polygon(height_updated[0]).centroid.coords)[0]
    end = list(Polygon(height_updated[-1]).centroid.coords)[0]
    moved_plots = []
    for i in height_updated:
        mid = list(Polygon(i).centroid.coords)[0]
        x1, y1 = findIntersection(start[0], start[1], end[0], end[1], mid[0], -10 * mid[1], mid[0], 10 * mid[1])
        y_factor = y1 - mid[1]
        if y_factor > 20 or y_factor < -20:
            change = [

                [i[0][0], i[0][1] + y_factor],
                [i[1][0], i[1][1] + y_factor],
                [i[2][0], i[2][1] + y_factor],
                [i[3][0], i[3][1] + y_factor],
                [i[4][0], i[4][1] + y_factor]

            ]
            moved_plots.append(change)
        else:
            moved_plots.append(i)
    return moved_plots


def Plot_adjuster(height_updated, plot_mid_dist):
    height_updated = sorted(height_updated)

    adjusted_plots = []
    for i in range(len(height_updated) - 1):
        p1 = list(Polygon(height_updated[i]).centroid.coords)[0]
        p2 = list(Polygon(height_updated[i + 1]).centroid.coords)[0]
        point_dist = round(math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2)))
        plot = height_updated[i + 1]
        if point_dist < plot_mid_dist * 0.9:
            x_factor = plot_mid_dist - point_dist
            change = [
                [plot[0][0] + x_factor, plot[0][1]],
                [plot[1][0] + x_factor, plot[1][1]],
                [plot[2][0] + x_factor, plot[2][1]],
                [plot[3][0] + x_factor, plot[3][1]],
                [plot[4][0] + x_factor, plot[4][1]]

            ]
            adjusted_plots.append(change)
        # elif plot_mid_dist * 1 < point_dist < plot_mid_dist * 1.4:
        #     x_factor = point_dist- plot_mid_dist
        #     change = [
        #         [plot[0][0] + x_factor, plot[0][1]],
        #         [plot[1][0] + x_factor, plot[1][1]],
        #         [plot[2][0] + x_factor, plot[2][1]],
        #         [plot[3][0] + x_factor, plot[3][1]],
        #         [plot[4][0] + x_factor, plot[4][1]]
        #
        #     ]
        #     adjusted_plots.append(change)

        else:
            adjusted_plots.append(plot)
    adjusted_plots.append(height_updated[0])

    height_updated = sorted(adjusted_plots)
    return height_updated

def Overlap_remover(height_updated):
    pixel_coords_H_move_updated = sorted(height_updated)

    pixel_coords_overlap_updated = []
    for i in range(len(pixel_coords_H_move_updated)):
        try:
            plot = pixel_coords_H_move_updated[i]

            if not i == len(pixel_coords_H_move_updated) - 1:
                adjacent_plot = pixel_coords_H_move_updated[i + 1]
                h, w = find_h_w(plot)
                p1 = Polygon(plot)
                p2 = Polygon(adjacent_plot)
                # reduce_percent = round((30 / 100) * w)

                if p1.intersects(p2):

                    p3 = p1.intersection(p2).area
                    # print(p3)
                     # pixel_coords_H_move_updated.remove(pixel_coords_H_move_updated[i + 1])
                else:
                    pixel_coords_overlap_updated.append(plot)

            else:
                pixel_coords_overlap_updated.append(plot)

        except Exception as e:
            print(e)

    return pixel_coords_overlap_updated

def range_to_range_Adjustment(range1, range2):
    # range1 = updated_plots[0]
    # range2 = updated_plots[1]
    range1 = sorted(range1)
    range2 = sorted(range2)

    updated_plot = []
    for i in range(len(range1)):
        try:
            p1 = range1[i]
            point1 = p1[3]

            p2 = range2[i]
            point2 = p2[0]
            intersect_ppint = [point1[0], point2[1]]
            space = round(math.sqrt(((point1[0] - intersect_ppint[0]) ** 2) + ((point1[1] - intersect_ppint[1]) ** 2)))
            # print(space)
            if space > 33:
                y_factor = space - 33
                change = [
                    [p2[0][0], p2[0][1] - y_factor],
                    [p2[1][0], p2[1][1] - y_factor],
                    [p2[2][0], p2[2][1] - y_factor],
                    [p2[3][0], p2[3][1] - y_factor],
                    [p2[4][0], p2[4][1] - y_factor]

                ]
                updated_plot.append(change)
            elif space < 33:
                y_factor = 33 - space
                change = [
                    [p2[0][0], p2[0][1] + y_factor],
                    [p2[1][0], p2[1][1] + y_factor],
                    [p2[2][0], p2[2][1] + y_factor],
                    [p2[3][0], p2[3][1] + y_factor],
                    [p2[4][0], p2[4][1] + y_factor]

                ]
                updated_plot.append(change)
            else:
                updated_plot.append(p2)
        except Exception as e:
            print(e, 'plot adjustment range to range')

    return updated_plot


def range_to_range_overlap(range1, range2):
    updated_plots = []
    for i in range1:
        p1 = Polygon(i)
        h, w = find_h_w(i)
        plot = i

        no_of_intersects = []

        for j in range2:
            p2 = Polygon(j)

            if p1.intersects(p2):
                p3 = p1.intersection(p2).area
                p1_a = p1.area
                overlap_percet = ((p3 / p1_a) * 100) + 5
                reduce_percent = round((overlap_percet / 100) * h)
                no_of_intersects.append(reduce_percent)

        try:
            reduce_percent = max(no_of_intersects)
            # print(no_of_intersects)
            change = [
                i[0],
                [i[1][0], i[1][1]],
                [i[2][0], i[2][1] - reduce_percent],
                [i[3][0], i[3][1] - reduce_percent],
                i[4]
            ]
            plot = change
            updated_plots.append(plot)
        except:
            updated_plots.append(plot)
            pass

    return updated_plots

import cv2
import geo
import numpy as np
from matplotlib import pyplot as plt
from skimage.io import imread
from osgeo import gdal
from pyproj import Proj, transform
import simplekml
import math
from shapely.geometry import Polygon
import statistics
import numpy as np
import pandas as pd

kml_path = '/mnt/dash/Alpha_Share/syngenta/Mock_Run/SweetCorn/CLuster_kml/cluster.kml'
tif_path = '/mnt/dash/Alpha_Share/syngenta/Syngenta_Groton_Sd_V8/Syngenta_Groton_Sd_V8_images_Updated_transparent_mosaic_group1.tif'

line = geo.kread(kml_path)

ds = gdal.Open(tif_path)
prj = ds.GetProjection()
geoTrans = ds.GetGeoTransform()
projection = geo.get_projection(tif_path)
# projection = '32614'
outProj = Proj(init='epsg:' + projection)
inProj = Proj(init='epsg:4326')

line_coords = []
for i in line:
    temp = []
    for j in i:
        x, y = geo.set_crs(j[0], j[1], '4326', projection)
        x, y = geo.world2Pixel(geoTrans, x, y)
        temp.append([x, y])
    line_coords.append(temp)

line_coords = sorted(line_coords)
from shapely.geometry import Polygon
from shapely.geometry import LineString


def alt_cluster(pixel_coords_m):

    cluster=[]
    for i in line_coords:
        line = LineString(i)
        temp=[]
        for j in pixel_coords_m:
            poly = Polygon(j)
            a = poly.exterior.intersection(line)
            if a:
                temp.append(j)
        cluster.append(temp)

    return cluster

def findIntersection(x1, y1, x2, y2, x3, y3, x4, y4):
    px = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / (
            (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))
    py = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / (
            (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))
    return [px, py]

def Stright_liner(coords):
    cluster_list=[]
    for clust,line in zip(coords,line_coords):
        moved_plots=[]
        height_updated = clust
        height_updated = sorted(height_updated, key=lambda k: k[0][1])
        start = line[0]
        end = line[1]
        for i in height_updated:
            mid = list(Polygon(i).centroid.coords)[0]
            x1, y1 = findIntersection(start[0], start[1], end[0], end[1], -10000, mid[1], mid[0] * 100, mid[1])
            x_factor = x1 - mid[0]
            if x_factor > 5:
                change = [
                    [i[0][0] + x_factor, i[0][1]],
                    [i[1][0] + x_factor, i[1][1]],
                    [i[2][0] + x_factor, i[2][1]],
                    [i[3][0] + x_factor, i[3][1]],
                    [i[4][0] + x_factor, i[4][1]]

                ]
                moved_plots.append(change)
            else:
                moved_plots.append(i)
        cluster_list.append(moved_plots)

    return cluster_list



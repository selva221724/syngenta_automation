import cv2
import numpy as np
from matplotlib import pyplot as plt
from skimage.io import imread
from osgeo import gdal
from pyproj import Proj, transform
import simplekml
import math
from shapely.geometry import Polygon
import statistics
import scipy.cluster.hierarchy as hcluster
import numpy as np
from sklearn.cluster import KMeans
import pandas as pd
import geo

# ============================================= fill the corners ============================
aoi_kml = '/mnt/dash/Alpha_Share/syngenta/4th_July_Delivery/Brooking_late_season/4341 Brookings limt 2019 .kml'
tif_path = '/mnt/dash/Alpha_Share/syngenta/Syngenta_Brookings/UPDATED/result_resampled.tif'

kml = geo.kread(aoi_kml)

ds = gdal.Open(tif_path)
prj = ds.GetProjection()
geoTrans = ds.GetGeoTransform()
projection = geo.get_projection(tif_path)
# projection = '32614'
outProj = Proj(init='epsg:' + projection)
inProj = Proj(init='epsg:4326')

pixel_coords = []
for i in kml:
    temp = []
    for j in i:
        x, y = geo.set_crs(j[0], j[1], '4326', projection)
        # x,y = j[0],j[1]
        x, y = geo.world2Pixel(geoTrans, x, y)
        temp.append([x, y])
    pixel_coords.append(temp)

pixel_coords = pixel_coords[0]
sides = [[pixel_coords[i], pixel_coords[i + 1]] for i in range(len(pixel_coords) - 1)]


def findIntersection(x1, y1, x2, y2, x3, y3, x4, y4):
    px = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / (
            (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))
    py = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / (
            (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))
    return [px, py]


def fill_the_corner(updated_plots, plot_mid_dist, half_w, half_h):
    updated_plots = sorted(updated_plots)
    new_plots = []

    # =========================================left corner ================================

    points = []
    for side in sides:
        p1 = side[0]
        p2 = side[1]
        plot = list(Polygon(updated_plots[0]).centroid.coords)[0]
        x1, y1 = findIntersection(p1[0], p1[1], p2[0], p2[1], plot[0] * -5, plot[1], plot[0], plot[1])
        points.append([x1, y1])

    index = np.argmin(np.sum((np.array(points) - np.array(plot)) ** 2, axis=1))
    intersection_point = points[index]
    p1 = plot
    p2 = intersection_point
    point_dist = round(math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2)))
    if point_dist > plot_mid_dist * 1.5:
        no_plots = int((point_dist / plot_mid_dist) + 1)
        if not no_plots == 1:
            no_plots = no_plots - 1
        if not no_plots == 0:
            point = p1[0]
            new_mids = []
            for plot in range(no_plots):
                x, y = point - plot_mid_dist, p1[1]
                try:
                    x1, y1 = findIntersection(p1[0], p1[1], p2[0], p2[1], x, y * -10, x, y * 10)
                except:
                    x1, y1 = x, y
                point -= plot_mid_dist
                new_mids.append([x, y1])

            for mid in new_mids:
                change = [
                    [mid[0] - half_w, mid[1] - half_h],
                    [mid[0] + half_w, mid[1] - half_h],
                    [mid[0] + half_w, mid[1] + half_h],
                    [mid[0] - half_w, mid[1] + half_h],
                    [mid[0] - half_w, mid[1] - half_h]
                ]
                new_plots.append(change)
    # =========================================right corner ================================
    points = []
    for side in sides:
        p1 = side[0]
        p2 = side[1]
        plot = list(Polygon(updated_plots[-1]).centroid.coords)[0]
        x1, y1 = findIntersection(p1[0], p1[1], p2[0], p2[1], plot[0] * -5, plot[1], plot[0], plot[1])
        points.append([x1, y1])

    index = np.argmin(np.sum((np.array(points) - np.array(plot)) ** 2, axis=1))
    intersection_point = points[index]
    p1 = plot
    p2 = intersection_point
    point_dist = round(math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2)))
    if point_dist > plot_mid_dist * 1.5:
        no_plots = int((point_dist / plot_mid_dist) + 1)
        if not no_plots == 1:
            no_plots = no_plots - 1
        if not no_plots == 0:
            point = p1[0]
            new_mids = []
            for plot in range(no_plots):
                x, y = point + plot_mid_dist, p1[1]
                try:
                    x1, y1 = findIntersection(p1[0], p1[1], p2[0], p2[1], x, y * -10, x, y * 10)
                except:
                    x1, y1 = x, y
                point += plot_mid_dist
                new_mids.append([x, y1])

            for mid in new_mids:
                change = [
                    [mid[0] - half_w, mid[1] - half_h],
                    [mid[0] + half_w, mid[1] - half_h],
                    [mid[0] + half_w, mid[1] + half_h],
                    [mid[0] - half_w, mid[1] + half_h],
                    [mid[0] - half_w, mid[1] - half_h]
                ]
                new_plots.append(change)

    # print(len(new_plots))
    for i in new_plots:
        updated_plots.append(i)

    updated_plots = sorted(updated_plots)
    return updated_plots


def fill_the_corner_sweet(updated_plots, plot_mid_dist, half_w, half_h):
    updated_plots = sorted(updated_plots, key=lambda k: k[0][1])
    new_plots = []

    # =========================================top corner ================================

    points = []
    for side in sides:
        p1 = side[0]
        p2 = side[1]
        plot = list(Polygon(updated_plots[0]).centroid.coords)[0]
        try:
            x1, y1 = findIntersection(p1[0], p1[1], p2[0], p2[1], plot[0], plot[1] * -5, plot[0], plot[1])
            points.append([x1, y1])
        except:
            a=None


    index = np.argmin(np.sum((np.array(points) - np.array(plot)) ** 2, axis=1))
    intersection_point = points[index]
    p1 = plot
    p2 = intersection_point
    point_dist = round(math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2)))
    if point_dist > plot_mid_dist * 1.3:
        no_plots = int((point_dist / plot_mid_dist) + 1)
        if not no_plots == 1:
            no_plots = no_plots - 1
        if not no_plots == 0:
            point = p1[1]
            new_mids = []
            for plot in range(no_plots):
                x, y = p1[0], point - plot_mid_dist
                try:
                    x1, y1 = findIntersection(p1[0], p1[1], p2[0], p2[1], x, y*-10, x, y*10)
                except:
                    x1, y1 = x, y
                point -= plot_mid_dist
                new_mids.append([x1, y])

            for mid in new_mids:
                change = [
                    [mid[0] - half_w, mid[1] - half_h],
                    [mid[0] + half_w, mid[1] - half_h],
                    [mid[0] + half_w, mid[1] + half_h],
                    [mid[0] - half_w, mid[1] + half_h],
                    [mid[0] - half_w, mid[1] - half_h]
                ]
                new_plots.append(change)
    # =========================================bottom corner ================================
    points = []
    for side in sides:
        p1 = side[0]
        p2 = side[1]
        plot = list(Polygon(updated_plots[-1]).centroid.coords)[0]
        try:
            x1, y1 = findIntersection(p1[0], p1[1], p2[0], p2[1], plot[0], plot[1] * 5, plot[0], plot[1])
            points.append([x1, y1])
        except:
            a=None

    index = np.argmin(np.sum((np.array(points) - np.array(plot)) ** 2, axis=1))
    intersection_point = points[index]
    p1 = plot
    p2 = intersection_point
    point_dist = round(math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2)))
    if point_dist > plot_mid_dist * 1.3:
        no_plots = int((point_dist / plot_mid_dist) + 1)
        if not no_plots == 1:
            no_plots = no_plots - 1
        if not no_plots == 0:
            point = p1[1]
            new_mids = []
            for plot in range(no_plots):
                x, y = p1[0], point + plot_mid_dist
                try:
                    x1, y1 = findIntersection(p1[0], p1[1], p2[0], p2[1], x, y * -10, x, y * 10)
                except:
                    x1, y1 = x, y
                point += plot_mid_dist
                new_mids.append([x1, y])

            for mid in new_mids:
                change = [
                    [mid[0] - half_w, mid[1] - half_h],
                    [mid[0] + half_w, mid[1] - half_h],
                    [mid[0] + half_w, mid[1] + half_h],
                    [mid[0] - half_w, mid[1] + half_h],
                    [mid[0] - half_w, mid[1] - half_h]
                ]
                new_plots.append(change)

    for i in new_plots:
        updated_plots.append(i)

    updated_plots = sorted(updated_plots, key=lambda k: k[0][1])
    return updated_plots

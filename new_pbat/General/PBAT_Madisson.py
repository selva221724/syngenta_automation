import geo
from pykml import parser
import re
import numpy as np
from matplotlib import pyplot as plt
import simplekml
from shapely.geometry import Point
import pyproj
from pyproj import Proj, transform
from osgeo import gdal
import madisson_support as pbat
from shapely.geometry import Polygon
from pandas import DataFrame
from statistics import mean
import fill_the_corners as fc

kml_path = '/mnt/dash/Alpha_Share/syngenta/4th_July_Delivery/Brooking_late_season/kml/1.kml'
tif_path = '/mnt/dash/Alpha_Share/syngenta/Syngenta_Brookings/UPDATED/result_resampled.tif'
rangerow = 96

kml = geo.kread(kml_path)

ds = gdal.Open(tif_path)
prj = ds.GetProjection()
geoTrans = ds.GetGeoTransform()
projection = geo.get_projection(tif_path)
# projection = '32614'
outProj = Proj(init='epsg:' + projection)
inProj = Proj(init='epsg:4326')

pixel_coords = []
for i in kml:
    temp = []
    for j in i:
        x, y = geo.set_crs(j[0], j[1], '4326', projection)
        # x,y = j[0],j[1]
        x, y = geo.world2Pixel(geoTrans, x, y)
        temp.append([x, y])
    pixel_coords.append(temp)

pixel_coords_m = []
for i in pixel_coords:
    x = [j[0] for j in i]
    y = [j[1] for j in i]
    xmin, ymin = min(x), min(y)
    xmax, ymax = max(x), max(y)
    pixel_coords_m.append([[xmin, ymin], [xmax, ymin],
                           [xmax, ymax], [xmin, ymax], [xmin, ymin]])

#     ===================================== finding the ======================================
line_kml = '/mnt/dash/Alpha_Share/syngenta/Mock_Run/Maddison/line_kmk/line.kml'
line = geo.kread(line_kml)

line_coords = []
for i in line:
    temp = []
    for j in i:
        x, y = geo.set_crs(j[0], j[1], '4326', projection)
        x, y = geo.world2Pixel(geoTrans, x, y)
        temp.append([x, y])
    line_coords.append(temp)

import math

p1 = line_coords[0][0]
p2 = line_coords[0][1]
range_len = round(math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2)))

cm_to_pix = range_len / 65.30153927881601  # one pixel covrage in cm
plot_length = 689
cm_to_pix= 230/512

plot_mid_dist = round(78 * cm_to_pix)
plot_len = round(plot_length * cm_to_pix)
plot_width = round(43 * cm_to_pix)


half_h = round((plot_len) / 2)
half_w = round(plot_width / 2)

# ========================= manual cluster =================================
# import Alt_Cluster
# cluster_list = Alt_Cluster.alt_cluster(pixel_coords_m)
# ================================================================


cluster_list = pbat.clustering_horizontal(pixel_coords_m, rangerow)
cluster_list = sorted(cluster_list, key=lambda k: k[0][0][1])  # sort the cluster in y axis
# cluster_list = [[cluster_list[0][-1]]]
# updated_plots = cluster_list
# ============================= find height and width and distance ===============

plot_dist = []
height =[]
width =[]
for i in cluster_list:
    i = sorted(i)
    for j in range(len(i)-1):
        p1 = list(Polygon(i[j]).centroid.coords)[0]
        p2 = list(Polygon(i[j+1]).centroid.coords)[0]
        distance = round(math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2)))
        plot_dist.append(distance)
    for j in i:
        h,w = pbat.find_h_w(j)
        height.append(h)
        width.append(w)

plot_dist = pbat.outlier_detection(plot_dist)
plot_dist = mean(pbat.outlier_detection(plot_dist))
height = mean(pbat.outlier_detection(height))
width = mean(pbat.outlier_detection(width))


half_h = height / 2
half_w = width / 2
plot_mid_dist = plot_dist

# =========================== Remove False Positive PLots =================================================

print('Remove False Positive PLots')
pixel_coords_updated = []
for i in cluster_list:
    change = pbat.false_poitive_remover(i, half_h, half_w)
    pixel_coords_updated.append(change)

updated_plots = sorted(pixel_coords_updated, key=lambda k: k[0][0][1])



# # # ============================================== Width Equalizer =================================

# half_w = half_w -5
print('Width Equalizer')
pixel_coords_updated = []
for i in updated_plots:
    change = pbat.width_equalizer(i, half_w)
    pixel_coords_updated.append(change)

updated_plots = pixel_coords_updated

# # ============================================= Shift Adjustment =====================================

# print('Up & Down Shift Adjustment')
# shifted = []
# for i in updated_plots:
#     shifted.append(pbat.Stright_liner(i))
#
# updated_plots = shifted

# ============================================= shift adjustment manual cluster==========================
# import Alt_Cluster
# shifted = Alt_Cluster.Stright_liner(updated_plots)
# updated_plots = shifted

#
# # =========================================plot adjuster ================================
# shifted=[]
# for i in updated_plots:
#     shifted.append(pbat.Plot_adjuster(i, plot_mid_dist))
#
# updated_plots = shifted

# # ====================================== overlap =======================================
print('overlap polygons removing')
overlap = []
for i in updated_plots:
    overlap.append(pbat.Overlap_remover(i))

updated_plots = overlap

# # ======================================= Fill the gap ===========================================

print('fill the gap')
fill = []
for i in updated_plots:
    fill.append(pbat.fill_the_gaps(i, plot_mid_dist, half_w, half_h))

updated_plots = fill
#
# # # ============================================= fill the corners ============================
print('fill the corners')
fill=[]
for i in updated_plots:
    change = fc.fill_the_corner(i,plot_mid_dist,half_w,half_h)
    fill.append(change)

updated_plots = fill

# # ====================================== overlap =======================================
print('overlap polygons removing')
overlap = []
for i in updated_plots:
    overlap.append(pbat.Overlap_remover(i))

updated_plots = overlap

# # =============================================== range to range adjustment ===============
# # print('range_to_range_Adjustment')
# #
# # range_updated =[]
# # for i in range(len(updated_plots)-1):
# #     chnage = pbat.range_to_range_Adjustment(updated_plots[i],updated_plots[i+1])
# #     range_updated.append(chnage)
# #
# # range_updated.append(updated_plots[0])
# #
# # updated_plots = range_updated
# # #
# # # ============================================== Overlap alignment between Ranges =================================

print('range_to_range_overlap adjustment')

range_overlap_updated = []
for i in range(len(updated_plots) - 1):
    chnage = pbat.range_to_range_overlap(updated_plots[i], updated_plots[i + 1])
    range_overlap_updated.append(chnage)
range_overlap_updated.append(updated_plots[-1])

updated_plots = range_overlap_updated

# # ================================================== declustering =====================================

pixel_coords_updated_1 = updated_plots
final_coords = []
for i in pixel_coords_updated_1:
    for plot in i:
        final_coords.append(plot)
# ================================================================================================

global_coords = []
for i in final_coords:
    temp = []
    for j in i:
        x, y = geo.Pixel2world(geoTrans, j[0], j[1])
        x, y = geo.set_crs(x, y, projection, '4326')
        temp.append([x, y])
    global_coords.append(temp)

geo.kgen(global_coords, 'out.kml')

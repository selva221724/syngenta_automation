import cv2
import numpy as np
from matplotlib import pyplot as plt
from skimage.io import imread
from osgeo import gdal
from pyproj import Proj, transform
import simplekml
import math
from shapely.geometry import Polygon
import statistics
import scipy.cluster.hierarchy as hcluster
import numpy as np
from sklearn.cluster import KMeans
import pandas as pd


# ========================================= Clustering ==============================================

def findIntersection(x1, y1, x2, y2, x3, y3, x4, y4):
    px = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / (
            (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))
    py = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / (
            (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))
    return [px, py]


def find_h_w(a):
    temp = []
    for i in range(len(a) - 1):
        p1 = a[i]
        p2 = a[i + 1]
        distance = round(math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2)))
        temp.append(distance)

    width = min(temp)
    height = max(temp)

    return height, width


def outlier_detection(f):
    df_in = pd.DataFrame(f, columns=['a'])

    q1 = df_in['a'].quantile(0.25)
    q3 = df_in['a'].quantile(0.75)
    iqr = q3 - q1  # Interquartile range
    fence_low = q1 - 1.5 * iqr
    fence_high = q3 + 1.5 * iqr
    df_out = df_in.loc[(df_in['a'] > fence_low) & (df_in['a'] < fence_high)]
    if not list(df_out['a']):
        return f
    else:
        return list(df_out['a'])


def avrg_plot_distance_finder(a):
    temp = []
    for i in range(len(a) - 1):
        p1 = a[i]
        p2 = a[i + 1]
        distance = round(math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2)))
        temp.append(distance)

    temp = outlier_detection(temp)
    avrg = statistics.mean(temp)

    return avrg


def plot_distance_finder(a):
    temp = []
    for i in range(len(a) - 1):
        p1 = a[i]
        p2 = a[i + 1]
        distance = round(math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2)))
        temp.append(distance)

    # temp = outlier_detection(temp)
    # avrg = statistics.mean(temp)

    return temp


def clustering_vertical(pixel_coords_m, number_of_cluster):
    imcentx = [i[0][0] for i in pixel_coords_m]
    imcenty = [i[0][1] for i in pixel_coords_m]

    import scipy.cluster.hierarchy as hcluster
    import numpy as np
    from sklearn.cluster import KMeans
    imcentx = np.array(imcentx)

    kmeans = KMeans(init='k-means++', n_clusters=number_of_cluster, max_iter=500).fit(imcentx.reshape(-1, 1))

    id_label = kmeans.labels_
    # plt.scatter(imcenty,imcentx,c=kmeans.labels_)
    # plt.show()

    ids = list(id_label)

    cluster_list = []
    for i in range(number_of_cluster):
        temp = []
        for j, k in zip(pixel_coords_m, ids):
            if k == i:
                temp.append(j)
        cluster_list.append(temp)

    return cluster_list


def false_poitive_remover(pixel_coords_m, half_h, half_w):
    updated_plots = []
    for i in pixel_coords_m:
        h, w = find_h_w(i)
        if h < (half_h * 2) * 0.8:
            continue
        elif h > (half_h * 2) * 1.2:
            continue
        elif w < (half_w * 2) * 0.8:
            continue
        elif w > (half_w * 2) * 1.2:
            continue
        else:
            # print(i)
            updated_plots.append(i)

    updated_plots = sorted(updated_plots, key=lambda k: k[0][1])

    return updated_plots


def width_equalizer(pixel_coords_m, half_w):
    # =================================== Width Adjustment ========================================

    height_updated = []
    for i in pixel_coords_m:
        mid = list(Polygon(i).centroid.coords)[0]
        mid = [round(mid[0]), round(mid[1])]
        change = [
            [i[0][0], mid[1] - half_w],
            [i[1][0], mid[1] - half_w],
            [i[2][0], mid[1] + half_w],
            [i[3][0], mid[1] + half_w],
            [i[4][0], mid[1] - half_w]
        ]
        height_updated.append(change)

    return height_updated


def fill_the_gaps(height_updated, plot_mid_dist, half_w, half_h):
    new_plots = []
    for i in range(len(height_updated) - 1):
        p1 = list(Polygon(height_updated[i]).centroid.coords)[0]
        p2 = list(Polygon(height_updated[i + 1]).centroid.coords)[0]
        point_dist = round(math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2)))
        if point_dist > plot_mid_dist * 1.5:
            no_plots = int((point_dist / plot_mid_dist) +0.5)
            if not no_plots == 1:
                no_plots = no_plots - 1
            if not no_plots == 0:
                point = p1[1]
                new_mids = []
                for plot in range(no_plots):
                    x, y = p1[0], plot_mid_dist + point
                    try:
                        x1, y1 = findIntersection(p1[0], p1[1], p2[0], p2[1], -10000, y, x * 100, y)
                    except:
                        x1, y1 = x, y
                    point += plot_mid_dist
                    new_mids.append([x1, y])

                for mid in new_mids:
                    change = [
                        [mid[0] - half_w, mid[1] - half_h],
                        [mid[0] + half_w, mid[1] - half_h],
                        [mid[0] + half_w, mid[1] + half_h],
                        [mid[0] - half_w, mid[1] + half_h],
                        [mid[0] - half_w, mid[1] - half_h]
                    ]
                    new_plots.append(change)

    for i in new_plots:
        height_updated.append(i)

    height_updated = sorted(height_updated, key=lambda k: k[0][1])

    return height_updated


def Stright_liner(height_updated):
    height_updated = sorted(height_updated, key=lambda k: k[0][1])
    start = list(Polygon(height_updated[0]).centroid.coords)[0]
    end = list(Polygon(height_updated[-1]).centroid.coords)[0]
    moved_plots = []
    for i in height_updated:
        mid = list(Polygon(i).centroid.coords)[0]
        x1, y1 = findIntersection(start[0], start[1], end[0], end[1], -10000, mid[1], mid[0] * 100, mid[1])
        x_factor = x1 - mid[0]
        if x_factor > 20 or x_factor < -20:
            change = [
                [i[0][0] + x_factor, i[0][1]],
                [i[1][0] + x_factor, i[1][1]],
                [i[2][0] + x_factor, i[2][1]],
                [i[3][0] + x_factor, i[3][1]],
                [i[4][0] + x_factor, i[4][1]]

            ]
            moved_plots.append(change)
        else:
            moved_plots.append(i)

    return moved_plots


def Plot_adjuster(height_updated, plot_mid_dist):
    height_updated = sorted(height_updated, key=lambda k: k[0][1])

    adjusted_plots = []
    for i in range(len(height_updated) - 1):
        p1 = list(Polygon(height_updated[i]).centroid.coords)[0]
        p2 = list(Polygon(height_updated[i + 1]).centroid.coords)[0]
        point_dist = round(math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2)))
        plot = height_updated[i + 1]
        if point_dist < plot_mid_dist * 0.9:
            y_factor = plot_mid_dist - point_dist
            change = [
                [plot[0][0], plot[0][1] + y_factor],
                [plot[1][0], plot[1][1] + y_factor],
                [plot[2][0], plot[2][1] + y_factor],
                [plot[3][0], plot[3][1] + y_factor],
                [plot[4][0], plot[4][1] + y_factor]

            ]
            adjusted_plots.append(change)
        else:
            adjusted_plots.append(plot)
    adjusted_plots.append(height_updated[0])

    height_updated = sorted(adjusted_plots, key=lambda k: k[0][1])
    return height_updated

def Overlap_remover(pixel_coords_H_move_updated):
    # pixel_coords_H_move_updated = sorted(height_updated)

    pixel_coords_overlap_updated = []
    for i in range(len(pixel_coords_H_move_updated)):
        try:
            plot = pixel_coords_H_move_updated[i]

            if not i == len(pixel_coords_H_move_updated) - 1:
                adjacent_plot = pixel_coords_H_move_updated[i + 1]
                h, w = find_h_w(plot)
                p1 = Polygon(plot)
                p2 = Polygon(adjacent_plot)
                # reduce_percent = round((30 / 100) * w)

                if p1.intersects(p2):

                    p3 = p1.intersection(p2).area
                    # print(p3)
                     # pixel_coords_H_move_updated.remove(pixel_coords_H_move_updated[i + 1])
                else:
                    pixel_coords_overlap_updated.append(plot)

            else:
                pixel_coords_overlap_updated.append(plot)

        except Exception as e:
            print(e)

    return pixel_coords_overlap_updated

def range_to_range_Adjustment(range1, range2):
    # range1 = updated_plots[0]
    # range2 = updated_plots[1]

    updated_plot = []
    for i in range(len(range1)):
        try:
            p1 = range1[i]
            point1 = p1[1]

            p2 = range2[i]
            point2 = p2[0]
            intersect_ppint = [point2[0], point1[1]]
            space = round(math.sqrt(((point1[0] - intersect_ppint[0]) ** 2) + ((point1[1] - intersect_ppint[1]) ** 2)))
            # print(space)
            if space > 9:
                x_factor = space - 9
                change = [
                    [p2[0][0] - x_factor, p2[0][1]],
                    [p2[1][0] - x_factor, p2[1][1]],
                    [p2[2][0] - x_factor, p2[2][1]],
                    [p2[3][0] - x_factor, p2[3][1]],
                    [p2[4][0] - x_factor, p2[4][1]]

                ]
                updated_plot.append(change)
            elif space < 9:
                x_factor = 9 - space
                change = [
                    [p2[0][0] + x_factor, p2[0][1]],
                    [p2[1][0] + x_factor, p2[1][1]],
                    [p2[2][0] + x_factor, p2[2][1]],
                    [p2[3][0] + x_factor, p2[3][1]],
                    [p2[4][0] + x_factor, p2[4][1]]

                ]
                updated_plot.append(change)
            else:
                updated_plot.append(p2)
        except Exception as e:
            a = 1

    return updated_plot


def range_to_range_overlap(range1, range2):
    updated_plots = []
    for i in range1:
        p1 = Polygon(i)
        h, w = find_h_w(i)
        plot = i

        no_of_intersects = []

        for j in range2:
            p2 = Polygon(j)

            if p1.intersects(p2):
                p3 = p1.intersection(p2).area
                p1_a = p1.area
                overlap_percet = ((p3 / p1_a) * 100) + 10
                reduce_percent = round((overlap_percet / 100) * h)
                no_of_intersects.append(reduce_percent)

        try:
            reduce_percent = max(no_of_intersects)
            # print(no_of_intersects)
            change = [
                i[0],
                [i[1][0] - reduce_percent, i[1][1]],
                [i[2][0] - reduce_percent, i[2][1]],
                i[3],
                i[4]
            ]
            plot = change
            updated_plots.append(plot)
        except:
            updated_plots.append(plot)
            pass

    return updated_plots

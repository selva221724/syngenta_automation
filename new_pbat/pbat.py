import cv2
import numpy as np
from matplotlib import pyplot as plt
from skimage.io import imread
from osgeo import gdal
from pyproj import Proj, transform
import simplekml
import math
from shapely.geometry import Polygon
import statistics
import scipy.cluster.hierarchy as hcluster
import numpy as np
from sklearn.cluster import KMeans
import pandas as pd

# ========================================= Clustering ==============================================

def find_h_w(a):
    temp = []
    for i in range(len(a) - 1):
        p1 = a[i]
        p2 = a[i + 1]
        distance = round(math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2)))
        temp.append(distance)

    width = min(temp)
    height = max(temp)

    return height, width

def outlier_detection(f):
    df_in = pd.DataFrame(f, columns=['a'])

    q1 = df_in['a'].quantile(0.25)
    q3 = df_in['a'].quantile(0.75)
    iqr = q3 - q1  # Interquartile range
    fence_low = q1 - 1.5 * iqr
    fence_high = q3 + 1.5 * iqr
    df_out = df_in.loc[(df_in['a'] > fence_low) & (df_in['a'] < fence_high)]
    if not list(df_out['a']):
        return f
    else:
        return list(df_out['a'])


def avrg_plot_distance_finder(a):
    temp = []
    for i in range(len(a) - 1):
        p1 = a[i]
        p2 = a[i + 1]
        distance = round(math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2)))
        temp.append(distance)

    temp = outlier_detection(temp)
    avrg = statistics.mean(temp)

    return avrg

def plot_distance_finder(a):
    temp = []
    for i in range(len(a) - 1):
        p1 = a[i]
        p2 = a[i + 1]
        distance = round(math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2)))
        temp.append(distance)

    # temp = outlier_detection(temp)
    # avrg = statistics.mean(temp)

    return temp




def clustering_horizontal(pixel_coords_m, number_of_cluster):
    imcentx = [i[0][0] for i in pixel_coords_m]
    imcenty = [i[0][1] for i in pixel_coords_m]


    imcenty = np.array(imcenty)

    kmeans = KMeans(init='k-means++', n_clusters=number_of_cluster, max_iter=500).fit(imcenty.reshape(-1, 1))
    id_label = kmeans.labels_
    # plt.scatter(imcentx,imcenty,c=kmeans.labels_)
    # plt.show()

    ids = list(id_label)

    cluster_list = []
    for i in range(number_of_cluster):
        temp = []
        for j, k in zip(pixel_coords_m, ids):
            if k == i:
                temp.append(j)
        cluster_list.append(temp)

    return cluster_list


def clustering_vertical(pixel_coords_m, number_of_cluster):
    imcentx = [i[0][0] for i in pixel_coords_m]
    imcenty = [i[0][1] for i in pixel_coords_m]

    import scipy.cluster.hierarchy as hcluster
    import numpy as np
    from sklearn.cluster import KMeans
    imcentx = np.array(imcentx)

    kmeans = KMeans(init='k-means++', n_clusters=number_of_cluster, max_iter=500).fit(imcentx.reshape(-1, 1))
    id_label = kmeans.labels_
    # plt.scatter(imcenty,imcentx,c=kmeans.labels_)
    # plt.show()

    ids = list(id_label)

    cluster_list = []
    for i in range(number_of_cluster):
        temp = []
        for j, k in zip(pixel_coords_m, ids):
            if k == i:
                temp.append(j)
        cluster_list.append(temp)

    return cluster_list


def magic_plot_horizontal(pixel_coords_m):
    W = []
    H = []
    for i in pixel_coords_m:
        h, w = find_h_w(i)
        W.append(w)
        H.append(h)

    # =================================== Width Adjustment ========================================
    # W = outlier_detection(W)
    reduce_percent = 25
    avrage_w = round(statistics.mean(W))
    avrage_w = avrage_w - round((reduce_percent / 100) * avrage_w)

    pixel_coords_W_updated = []
    for i in pixel_coords_m:
        h, w = find_h_w(i)
        if w > avrage_w:
            change = [i[0],
                      [i[0][0] + avrage_w, i[1][1]],
                      [i[0][0] + avrage_w, i[2][1]],
                      i[3],
                      i[4]]
            pixel_coords_W_updated.append(change)
        else:
            pixel_coords_W_updated.append(i)

    # print(len(pixel_coords_W_updated))


    # ======================================= Height Adjustment =======================================================
    # H = outlier_detection(H)
    reduce_percent = 0
    avrage_h = round(statistics.mean(H))
    avrage_h = avrage_h - round((reduce_percent / 100) * avrage_h)

    below_percent = avrage_h -round((10 / 100) * avrage_h)

    pixel_coords_H_updated = []
    for i in pixel_coords_W_updated:
        h, w = find_h_w(i)
        if h > avrage_h:
            change = [i[0],
                      i[1],
                      [i[2][0], i[1][1] + avrage_h],
                      [i[0][0], i[0][1] + avrage_h],
                      i[4]]
            pixel_coords_H_updated.append(change)
        elif h < below_percent:
            change = [i[0],
                      i[1],
                      [i[2][0], i[1][1] + avrage_h],
                      [i[0][0], i[0][1] + avrage_h],
                      i[4]]
            pixel_coords_H_updated.append(change)
            # print('yes')
        else:
            pixel_coords_H_updated.append(i)

    pixel_coords_H_updated = sorted(pixel_coords_H_updated)

    # print(len(pixel_coords_H_updated))

    # ===================================== move the plot in y axis ===============================

    plots_num = 20
    pixel_coords_H_move_updated = []
    for i in range(len(pixel_coords_H_updated)):
        plot = pixel_coords_H_updated[i]
        start_point = []
        if i < plots_num:
            for j in range(0, plots_num):
                poly = pixel_coords_H_updated[i + j]
                s_p = poly[0][1]
                start_point.append(s_p)
        elif i > len(pixel_coords_H_updated) - plots_num:
            for j in range(-plots_num, 0):
                poly = pixel_coords_H_updated[i + j]
                s_p = poly[0][1]
                start_point.append(s_p)
        else:
            for j in range(-plots_num, plots_num):
                poly = pixel_coords_H_updated[i + j]
                s_p = poly[0][1]
                start_point.append(s_p)

        # start_point = outlier_detection(start_point)
        avrg_start_point = round(statistics.mean(start_point))
        vari_percent = 10

        if not avrg_start_point - vari_percent < plot[0][1] < avrg_start_point + vari_percent:

            dif = avrg_start_point - plot[0][1]
            change = [[plot[0][0], avrg_start_point],
                      [plot[1][0], avrg_start_point],
                      [plot[2][0], plot[2][1] + dif],
                      [plot[3][0], plot[3][1] + dif],
                      [plot[0][0], avrg_start_point]]
            pixel_coords_H_move_updated.append(change)
        else:
            pixel_coords_H_move_updated.append(plot)

    # print(len(pixel_coords_H_move_updated))
    # ======================================== Overlapping Polygons Change=============================

    pixel_coords_overlap_updated = []
    for i in range(len(pixel_coords_H_move_updated)):
        plot = pixel_coords_H_move_updated[i]

        if not i == len(pixel_coords_H_move_updated) - 1:
            adjacent_plot = pixel_coords_H_move_updated[i + 1]
            h, w = find_h_w(plot)
            reduce_percent = round((30 / 100) * w)
            p1 = Polygon(plot)
            p2 = Polygon(adjacent_plot)
            if p1.intersects(p2):
                change = [plot[0],
                          [plot[1][0] - reduce_percent, plot[1][1]],
                          [plot[2][0] - reduce_percent, plot[2][1]],
                          plot[3],
                          plot[4]]
                pixel_coords_overlap_updated.append(change)
            else:
                pixel_coords_overlap_updated.append(plot)
        else:
            pixel_coords_overlap_updated.append(plot)

    # print(len(pixel_coords_overlap_updated))

    # ============================================ New Plots Adding ============================================
    import math
    coords = pixel_coords_overlap_updated
    coords_top = []
    dist=[]
    for i in range(len(coords) - 1):
        coords_top.append(coords[i][1])
        coords_top.append(coords[i + 1][0])

        p1 = coords[i][1]
        p2 = coords[i + 1][0]
        distance = round(math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2)))
        dist.append(distance)

    sumofdist = outlier_detection(dist)
    avrg_dist = statistics.mean(sumofdist)

    # avrg_dist = avrg_plot_distance_finder(coords_top)

    avrg_width = W[0]
    avrg_height = H[0]

    aco_len = avrg_dist + avrg_width + avrg_dist  # how much can be plotted

    import math
    new_plots = []
    for i in range(len(coords_top) - 1):
        p1 = coords_top[i]
        p2 = coords_top[i + 1]
        distance = round(math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2)))
        if distance > aco_len:
            no_of_plots = int((distance / aco_len)+0.5)
            local_dist= avrg_dist
            for plot in range(1,no_of_plots+1):
                px = [p1[0] + local_dist, p1[1]]
                local_dist += avrg_dist+avrg_width
                new = [
                    px,
                    [px[0] + avrg_width, px[1]],
                    [px[0] + avrg_width, px[1] + avrg_height],
                    [px[0], p1[1] + avrg_height],
                    px
                ]
                new_plots.append(new)

    for i in new_plots:
        pixel_coords_overlap_updated.append(i)

    return pixel_coords_overlap_updated


def magic_plot_vertical(pixel_coords_m):
    W = []
    H = []
    for i in pixel_coords_m:
        h, w = find_h_w(i)
        W.append(w)
        H.append(h)

    # =================================== Width Adjustment ========================================

    reduce_percent = 0
    avrage_w = round(statistics.mean(W))
    avrage_w = avrage_w - round((reduce_percent / 100) * avrage_w)

    pixel_coords_W_updated = []
    for i in pixel_coords_m:
        h, w = find_h_w(i)
        if w > avrage_w:
            change = [i[0],
                      [i[0][0] + avrage_w, i[1][1]],
                      [i[0][0] + avrage_w, i[2][1]],
                      i[3],
                      i[4]]
            pixel_coords_W_updated.append(change)
        else:
            pixel_coords_W_updated.append(i)

    # ======================================= Height Adjustment =======================================================

    reduce_percent = 5
    avrage_h = round(statistics.mean(H))
    avrage_h = avrage_h - round((reduce_percent / 100) * avrage_h)

    pixel_coords_H_updated = []
    for i in pixel_coords_W_updated:
        h, w = find_h_w(i)
        if h > avrage_h:
            change = [i[0],
                      i[1],
                      [i[2][0], i[1][1] + avrage_h],
                      [i[0][0], i[0][1] + avrage_h],
                      i[4]]
            pixel_coords_H_updated.append(change)
        else:
            pixel_coords_H_updated.append(i)

    pixel_coords_H_updated = sorted(pixel_coords_H_updated)

    # ===================================== move the plot in x axis ===============================

    plots_num = 10
    pixel_coords_H_move_updated = []
    for i in range(len(pixel_coords_H_updated)):
        plot = pixel_coords_H_updated[i]
        start_point = []
        if i < plots_num:
            for j in range(0, plots_num):
                poly = pixel_coords_H_updated[i + j]
                s_p = poly[0][0]
                start_point.append(s_p)
        elif i > len(pixel_coords_H_updated) - plots_num:
            for j in range(-plots_num, 0):
                poly = pixel_coords_H_updated[i + j]
                s_p = poly[0][0]
                start_point.append(s_p)
        else:
            for j in range(-plots_num, plots_num):
                poly = pixel_coords_H_updated[i + j]
                s_p = poly[0][0]
                start_point.append(s_p)

        avrg_start_point = round(statistics.mean(start_point))
        vari_percent = 10

        if not avrg_start_point - vari_percent < plot[0][0] < avrg_start_point + vari_percent:

            dif = avrg_start_point - plot[0][0]
            change = [[avrg_start_point,plot[0][1]],
                      [avrg_start_point,plot[1][1]],
                      [plot[2][1] + dif,plot[2][1]],
                      [plot[3][1] + dif,plot[3][1]],
                      [avrg_start_point,plot[0][1]]]

            pixel_coords_H_move_updated.append(change)
        else:
            pixel_coords_H_move_updated.append(plot)

    # ======================================== Overlapping Polygons Change=============================

    pixel_coords_overlap_updated = []
    for i in range(len(pixel_coords_H_move_updated)):
        if not i == len(pixel_coords_H_move_updated) - 1:
            plot = pixel_coords_H_move_updated[i]
            adjacent_plot = pixel_coords_H_move_updated[i + 1]
            h, w = find_h_w(plot)
            reduce_percent = round((30 / 100) * w)
            p1 = Polygon(plot)
            p2 = Polygon(adjacent_plot)
            if p1.intersects(p2):
                change = [
                    plot[0],
                    plot[1],
                    [plot[2][0], plot[2][1] - reduce_percent],
                    [plot[3][0],plot[3][1]-reduce_percent],
                    plot[4]
                ]
                pixel_coords_overlap_updated.append(change)
            else:
                pixel_coords_overlap_updated.append(plot)
        else:
            pixel_coords_overlap_updated.append(plot)

    return pixel_coords_overlap_updated